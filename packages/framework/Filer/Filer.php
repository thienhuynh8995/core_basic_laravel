<?php

namespace Gonosen\Filer;

use App;
use Gonosen\Filer\Traits\FileDisplay;
use Gonosen\Filer\Traits\Uploader;
use Gonosen\Filer\Form\Forms;

class Filer
{

    use FileDisplay, Uploader;

    public function __construct()
    {
        $this->image = App::make('image');
    }

}
