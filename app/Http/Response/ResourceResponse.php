<?php
namespace App\Http\Response;

use Illuminate\Support\Facades\View;
use App\Helpers\LayoutHelper;

class ResourceResponse extends Response
{
    public function output()
    {
        /*
        * Passing isAdmin to all view and layout
        */
        $isAdmin = \App\Helpers\AppHelper::isAdmin();
        View::share(['isAdmin'=>$isAdmin]);

        /*
        * Pass layoutHelper to all view and layout
        */
        View::share(['layoutHelper'=>new LayoutHelper()]);
        return parent::output();
    }
}
