<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LayoutHelper;

class NewsDetail extends Model
{
    //
    protected $table = 'news_detail';
    protected $fillable = ['title', 'content', 'lang_id'];

    /**
     * Update NewsDetail with Multi Languages
     *
     * @return \Illuminate\Http\Response
     */
    public function updateDataWithLang($post_data, $id=''){
        $layoutHelper = new LayoutHelper();
        foreach($post_data['title'] as $key => $item) {
            $match = $layoutHelper->checkMatchFormat($key);

            if ($match['status']) {
                $update_data = [
                    'title'=> $item,
                    'content' => $post_data['content'][$key]
                ];
                $this->where(['lang_id'=>$match['number'], 'news_id'=>$id])->update($update_data);
            } else {
                $added_data[] = [
                    'title'=> $item,
                    'content' => $post_data['content'][$key],
                    'lang_id' => $key,
                    'news_id' => $id
                ];
            }
        }
        if (!empty($added_data)){
            if ($this->insert($added_data)){
                return ['status'=>'success', 'message'=>'Update successfully'];
            } else {
                return ['status'=>'fail', 'message'=>'Update fail'];
            }
        }
        return ['status'=>'success', 'message'=>'Update successfully'];
    }
    public function languages()
    {
        return $this->hasOne('App\Models\Languages', 'id', 'lang_id');
    }
}
