<div class="row">
    <div class="col-lg-12 mt20">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $title }} Management
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Content here -->
                <div class="row mb10">
                    <div class="col-md-12 col-xs-12 txt_left">
                            <span class="mr05">
                                <button class="btn btn-default bggrey btn_checkall">Check all</button>
                            </span>
                            <span class="mr05">
                                <button class="btn btn-default bggrey btn_clearall">Uncheck all</button>
                            </span>
                            @if ($isAdmin)
                            <span class="mr05">
                                <button class="btn btn-default bggrey" id="deleteAll"><i class="fa fa-trash-o fa-fw"></i>Delete all</button>
                            </span>
                            @endif
                    </div>
                </div>
                <form method="POST"
                    enctype="multipart/form-data"
                    id="form-table">
                    {{ csrf_field() }}
                    <table width="100%" class="table general_tb table-striped table-bordered table-hover {{ @$main_name }}">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th class="center"></th>
                                <th class="center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($products->items() as $product)
                            <tr class="odd" data-id="{{ $product->id }}">
                                <td class="center" width="50"><input type="checkbox" class="check" name="id[]" value="{{ $product->id }}"></td>
                                <td>{{ $product->title }}</td>
                                <td class="center" width="100">
                                    <a href="{{route('admin.news.edit', $product->id)}}" class="btn btn-primary">Update</a>
                                </td>
                                <td class="center" width="100">
                                    <a href="{{ route('admin.news.delete', $product->id) }}" class="btn btn-danger delete-btn">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr class="odd">
                                <td colspan="4">There is no news available!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </form>
                <!-- pager -->
                <div class="row mt20 mb50">
                    <div class="col-md-6 col-xs-12 mt20 mb15 txt_left">
                        <a class='btn btn-primary' href="{{route('admin.news.create', [ 'is_new' => '1' ])}}">Create new</a>
                    </div>
                    <div class="col-md-6 col-xs-12 txt_left">
                        {!! $products->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>

@section('footer-script')
<script>
$(function(){
    $("#deleteAll").click(function(){
        let checked = false;
        let idArr = [];
        $('[name="id[]"]').each(function(i, e){
            let $this = $(e);
            if ($this.prop('checked')) {
                checked = true;
                idArr.push($this.val());
            }
        })
        if (checked && confirm('Are you sure to delete checked items ?')) {
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: "{{ route('admin.news.deleteAll') }}",
                data: {
                    idArr: idArr
                },
                dataType: "json",
                success: function (response) {
                    idArr.forEach(element => {
                        $('tr[data-id="'+element+'"]').remove();
                        if ($('tr').length == 0) {
                            $('.news tbody').append('<tr class="odd"><td colspan="5">There is no news available!</td></tr>');
                        }
                    });
                }
            });
        } else {
            alert('Please select news to delete');
        }
    })
})
</script>
@endsection
