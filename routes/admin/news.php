<?php

// Admin  routes  for common pages
Route::group([
    'namespace' => 'Admin',
    'prefix' => set_route_guard('web'),
], function () {
	Route::match(['get', 'post'], '/news/create', 'NewsController@create')->name('admin.news.create');
	Route::match(['get', 'post'],'/news/edit/{id}', 'NewsController@edit')->name('admin.news.edit');
    Route::match(['get', 'post'],'/news/delete/{id}', 'NewsController@delete')->name('admin.news.delete');
    Route::post('/news/delete-all', 'NewsController@deleteAll')->name('admin.news.deleteAll');
	Route::get('/news/index', 'NewsController@index')->name('admin.news.index');
});
