<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $table = 'news';
    protected $fillable = ['status', 'layout_status', 'image'];
    public $lang_id;
    
    const PRODUCT_TBL = 'news';
    const PRODUCT_DETAIL_TBL = 'news_detail';
    const FOREIGN_KEY = 'news_id';

    public function product_detail()
    {
        return $this->hasMany('App\Models\NewsDetail', 'news_id', 'id');
    }

    public function getProductsWithLang($lang_id = 1, $limit = 10){
        $result = $this->select([
            self::PRODUCT_TBL.'.id',
            self::PRODUCT_DETAIL_TBL.'.title',
            self::PRODUCT_DETAIL_TBL.'.lang_id', 'languages.status as lang_status',
            self::PRODUCT_TBL.'.status', self::PRODUCT_TBL.'.layout_status',
            self::PRODUCT_TBL.'.image',
        ])
        ->join(self::PRODUCT_DETAIL_TBL, self::FOREIGN_KEY, self::PRODUCT_TBL.'.id')
        ->join('languages', self::PRODUCT_DETAIL_TBL.'.lang_id', 'languages.id')
        ->where(['languages.status'=>1, 'lang_id' => $lang_id])->paginate($limit);
        
        return $result;
    }

    public function getProductDetailWithLang($id, $lang){
        return $this->with(['product_detail'=>function($q) use($id, $lang){
            $q->where('lang_id', $lang);
        }])->where('id', $id)->select([
            self::PRODUCT_TBL.'.id',
            self::PRODUCT_TBL.'.status', self::PRODUCT_TBL.'.layout_status',
            self::PRODUCT_TBL.'.image',
        ])->first();
    }

    public function getProductsWithId($id){
        $result = $this->select([
                    self::PRODUCT_DETAIL_TBL.'.*', 
                    self::PRODUCT_TBL.'.image',
                    self::PRODUCT_TBL.'.status', 
                    self::PRODUCT_TBL.'.layout_status',
                    'languages.status as lang_status'
                ])
            ->join(self::PRODUCT_DETAIL_TBL, self::PRODUCT_DETAIL_TBL . '.' . self::FOREIGN_KEY, self::PRODUCT_TBL.'.id')
            ->join('languages', self::PRODUCT_DETAIL_TBL.'.lang_id', 'languages.id')
            ->where([self::PRODUCT_TBL.'.id'=> $id, 'languages.status'=>1])->get();
        return $result;
    }
}
