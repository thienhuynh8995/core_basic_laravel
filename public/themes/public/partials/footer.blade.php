<footer>
    <div class="main">
        <div class="text_center">
            <h2>仕事市場</h2>
            <p class="btncontact"><a href=""><i class="fa fa-envelope"></i>お問い合わせ</a></p>
            <div class="navsp">
                <span class="menusp"><img src="/assets/common_img/iconmenu.png" alt="MENU" class="icon"></span>
                <div class="sub_menusp">
                    <p class="closesp"><img src="/assets/common_img/iconclose.png" alt="CLOSE" class="iconclose"></p>
                    <ul>
                        <li>
                            <span class="ac_title">仕事市場のサービス</span>
                            <ul class="ac_con">
                                <li><a href=""><i class="fa fa-angle-right"></i>お仕事探し</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>お祝い金制度について</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>ビジネスマッチング</a></li>
                                <!--<li class="benefits-link"><a href=""><i class="fa fa-angle-right"></i>大阪ではたらくメリット</a></li>-->
                            </ul>
                        </li>
                        <li>
                            <span class="ac_title">運営会社について</span>
                            <ul class="ac_con">
                                <li><a href=""><i class="fa fa-angle-right"></i>運営企業情報</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>よくある質問</a></li>
                                {{-- <li><a href="{{route('disclaimer')}}"><i class="fa fa-angle-right"></i>免責事項</a></li> --}}
                                <li><a href=""><i class="fa fa-angle-right"></i>プライバシーポリシー</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>利用規約</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="ac_title">会員ログイン/新規登録</span>
                            <ul class="ac_con">
                                <li><a href=""><i class="fa fa-angle-right"></i>マイページログイン</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>新規会員登録</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="ac_title">法人様はこちら</span>
                            <ul class="ac_con">
                                <li><a href=""><i class="fa fa-angle-right"></i>掲載ご希望企業様へ</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>企業様ログインページ</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>法人様アカウント発行フォーム</a></li>
                            </ul>
                        </li>
                        <li>
                            <span class="ac_title">お問い合わせ</span>
                            <ul class="ac_con">
                                <li><a href=""><i class="fa fa-angle-right"></i>総合お問い合わせフォーム</a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
            <p class="mt4per fz12">(C) <?=date('Y');?> Shigoto ichiba. All Right Reserved.</p>
        </div>
        <div class="page_top_cont noselect" style="bottom: 200px;">
            <div class="page_top">
                <div class="to_top" style="opacity: 1;">
                    <a href="#wrapper"><img src="/assets/common_img/ptop.png" width="60" height="60" alt="PAGE UP"></a>
                </div>
            </div>
        </div>
    </div>
</footer>
