<?php
namespace App\Helpers;

use App\Models\Languages;
use App\Http\Requests\Request;

class LayoutHelper{
    protected $lang;
    protected $attr;

    public function __construct(){
        $this->lang = Languages::all();
    }

    public function setAttr($attr){
        $this->attr = $attr;
        return $this;
    }

    public function setLang($lang){
        $this->lang = $lang;
        return $this;
    }

    public function getAttr(){
        return $this->attr;
    }

    public function getLang(){
        return $this->lang->toArray();
    }

    public function validate(Request $request){
        $request->validate([
            'value.*.*' => 'required'
        ]);
        return $this;
    }

    /**
    ***************************************************************************
    * Created: 2019/05/22
    * Check match format exists or not and return number of it
    ***************************************************************************
    * @author: Thien Huynh<h_thien@stagegroup.jp>
    * @return: [ status: boolean, number: int]
    *
    *
    ***************************************************************************
    */
    public function checkMatchFormat($string){
        $status['status'] = gettype($string) == 'string' && preg_match('/^(?=(id\-)(\d+))/', $string, $id);
        $status['number'] = -1;
        if ($status['status']) {
            $status['number'] = count($id) > 0 ? $id[count($id)-1] : -1;
        }
        return $status;
    }

    /**
    ***************************************************************************
    * Created: 2019/05/21
    * Get layout data with
    ***************************************************************************
    * @author: Thien Huynh<h_thien@stagegroup.jp>
    * @return:
    *
    ***************************************************************************
    */
    public function getLayoutData($product_id = '', $product_table){
        $temp_input = [];
        $temp_input_arr = [];

        // template block manage
        $data['lang'] = $this->lang->where('status', 1)->toArray();
        $data['type'] = request()->get('type') !== null ? request()->get('type') : (!empty($product_id) ? 'layout_content_management' : 'layout_management');
        return $data;
    }

    /**
    ***************************************************************************
    * Created: 2019/05/21
    * Update layout with post_data
    ***************************************************************************
    * @author: Thien Huynh<h_thien@stagegroup.jp>
    * @return:
    *
    ***************************************************************************
    */
    public function updateLayoutData($post_data, $type, $product_id, $product_table){
        $inputModel = new TemplateInput();
        $layout_input = isset($post_data['layout_input']) ? $post_data['layout_input'] : [];
        $layout_ordering = isset($post_data['layout_ordering']) ? $post_data['layout_ordering'] : '';
        $added_layout = [];

        foreach($layout_input as $key => $input) {
            if ($type == 'layout_content_management') {
                $lang_value = [];
                foreach($post_data['value'] as $value_type) {
                    if (isset($value_type[$key])) {
                        foreach ($value_type[$key] as $k => $vl) {
                            $lang_value['data_'.($k+1)] = $vl;
                        }
                    }
                }
                $inputModel->where('id', $key)->update($lang_value);
            } else {
                $match = $this->checkMatchFormat($key);
                if ($match['status']) {
                    $inputModel->where('id', $match['number'])->update(['ordering'=> $layout_ordering[$key]]);
                } else {
                    $added_layout[] = [
                        'product_id' => $product_id,
                        'product_table' => $product_table,
                        'template_block_id' => $input,
                        'ordering' => $layout_ordering[$key]
                    ];
                }
            }
        }
        if (!empty($added_layout)){
            if ($inputModel->insert($added_layout)){
                return ['status'=>'success', 'message'=>'Update successfully'];
            } else {
                return ['status'=>'fail', 'message'=>'Update fail'];
            }
        }
        return ['status'=>'success', 'message'=>'Update successfully'];
    }

    /**
    ***************************************************************************
    * Created: 2019/05/21
    * Render layout
    ***************************************************************************
    * @author: Thien Huynh<h_thien@stagegroup.jp>
    * @return:
    *
    ***************************************************************************
    */
    public function render($function=''){
        if (!empty($function) && method_exists($this, $function)){
            return $this->$function();
        } else {
            return '';
        }
    }

    /**
    ***************************************************************************
    * Created: 2019/05/21
    * Render function title
    ***************************************************************************
    * @author: Thien Huynh<h_thien@stagegroup.jp>
    * @return:
    *
    ***************************************************************************
    */
    protected function title(){
        $data = [
            'attr'=>$this->attr,
            'lang'=>$this->lang->where('status', 1)->all()
        ];
        $data['attr']['label'] = isset($data['attr']['label']) ? $data['attr']['label'] : 'Title';
        $html = view('Helpers.LayoutHelper.title', $data)->render();
        return $html;
    }

    /**
    ***************************************************************************
    * Created: 2019/05/21
    * Render function content_area
    ***************************************************************************
    * @author: Thien Huynh<h_thien@stagegroup.jp>
    * @return:
    *
    ***************************************************************************
    */
    protected function content_area(){
        $data = [
            'attr'=>$this->attr,
            'lang'=>$this->lang->where('status', 1)->all()
        ];
        $data['attr']['label'] = isset($data['attr']['label']) ? $data['attr']['label'] : 'Title';
        $html = view('Helpers.LayoutHelper.content_area', $data)->render();
        return $html;
    }
}
