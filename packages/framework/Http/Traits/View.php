<?php

namespace Gonosen\http\Traits;

trait View
{

    /**
     * @var  View for the response.
     */
    protected $view = null;

    protected $folder = "";

    /**
     * @return  View for the request
     */
    public function getView()
    {
        return $this->view;
    }

    public function setViewFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @param $view
     * @param null $includeFolder
     * @return self
     */
    public function view($view, $includeFolder = null)
    {
        $this->view = $view;

        if($this->folder) {
            $this->view = "$this->folder.$view";
        }

        if ($includeFolder) {
            $this->view = "$includeFolder.$view";
        }

        return $this;
    }

}
