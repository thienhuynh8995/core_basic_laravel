<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;
use App\Models\Languages;
use DebugBar\StandardDebugBar;

class AppHelper{
    const USER_ROLE_VISITOR=0;
    const USER_ROLE_NORMAL=1;
    const USER_ROLE_ADMIN=2;

    public static function getLocaleId($locale_type = 'vi'){

        if(\Session::has('locale'))
            $locale_type = \Session::get('locale');
            
        $language = Languages::where('type', $locale_type)->first();
        return isset($language->id) ? $language->id : 1;
    }

    public static function getLocaleName(){

        if(\Session::has('locale'))
            $locale_type = \Session::get('locale');
            
        $language = Languages::where('type', $locale_type)->first();
        return isset($language->name) ? $language->name : 'vi';
    }

    public static function getLocaleIdByName($name){
        $language = Languages::where('type', $name)->first();
        return isset($language->id) ? $language->id : 1;
    }

    public static function getLocaleIdDefault(){
        $name = 'en';
        $language = Languages::where('type', $name)->first();
        return isset($language->id) ? $language->id : 1;
    }

    public static function getPublicLangs(){
        return Languages::where('status', 1)->get()->toArray();
    }

    public static function isAdmin(){
        if(Auth::check()){
            if(Auth::user())
                return (Auth::user()->role==self::USER_ROLE_ADMIN);
        }
        return self::USER_ROLE_VISITOR;
    }

    /**
     * retur current route name
     * @return null|string
     */

    function currentRouteName()
    {
        $routeName = Route::currentRouteName();
        return $routeName;
    }

    public static function renderDebugbar() {
        if (getenv('APP_DEBUG') == 'true' || getenv('APP_DEBUG')){
            if (request()->isMethod('GET') && !request()->ajax()) {
                $debugbar = new StandardDebugBar();
                $renderer = $debugbar->getJavascriptRenderer()->render();
                echo $renderer;
            }
        }
    }

    public static function getSlug($text){
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = mb_convert_encoding($text, 'us-ascii', 'utf-8');
        // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    public static function getVisitorIp(){
        $fields = [];
        $fields[] = 'HTTP_CLIENT_IP';
        $fields[] = 'HTTP_X_CLIENT_IP';
        $fields[] = 'HTTP_X_FORWARDED_FOR';
        $fields[] = 'HTTP_X_FORWARDED';
        $fields[] = 'HTTP_FORWARDED_FOR';
        $fields[] = 'HTTP_FORWARDED';
        $fields[] = 'REMOTE_ADDR';

        $ipaddress = 'UNKNOWN';
        foreach($fields as $f){
            if (isset($_SERVER[$f])){
                $ipaddress = $_SERVER[$f];
                break;
            }
        }
        $ips = explode(':',$ipaddress);
        $ipaddress = $ips[0];
        return $ipaddress;
    }
    public static function getDeviceType(){
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
            return 'SP';
        return 'PC';
    }

    public static function hideNgWord($str)
    {
      $valueStr = is_array($str) ? implode(' ', $str) : $str;
      $valueStrToLower = strtolower($valueStr);
      $ngWords = array_map('strtolower', NgWord::pluck('name')->toArray());
      if (!empty($ngWords)) {
        foreach ($ngWords as $key => $val) {
          $strPos = strpos($valueStrToLower, $val);
          if ($strPos !== false) {
            $count = !empty(strlen($val)) ? strlen($val) : 0;
            $str = '';
            for ($i=0; $i<$count; $i++) {
              $str .= '*';
            }

            $valueStr = substr_replace($valueStr, $str, $strPos, $count);

            return self::hideNgWord($valueStr);
          }
        }
      }
      return $valueStr;
    }

    public static function getNewFileName($file) {
        $name = $file->getClientOriginalName();
        preg_match('/\.\w{3}$/', $name, $file_type);
        return time() . '-' . "file" . $file_type[0];
    }

    public static function convertCurrency($amount, $from, $to){

        // https://free.currencyconverterapi.com
        $json_data = @file_get_contents("https://free.currconv.com/api/v7/convert?q={$from}_{$to}&compact=ultra&apiKey=e2e2b234940b83161117");

        if($json_data != false){
            $data = json_decode($json_data);

            $property = $from . '_' . $to;

            return  number_format(round($amount * $data->$property, 3), 2);

        }

        return '';

    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function currency($amount, $to = ''){

        $default = config('constants.currency');

        if(\Session::has('currency'))
            $from = \Session::get('currency');

        if(empty($from))
            $from = $default;

        if(empty($to))
            $to = $default;

        if($from == $to)
            return $amount;

        // https://free.currencyconverterapi.com
        $json_data = @file_get_contents("https://free.currconv.com/api/v7/convert?q={$from}_{$to}&compact=ultra&apiKey=e2e2b234940b83161117");

        if($json_data != false){
            $data = json_decode($json_data);

            $property = $from . '_' . $to;

            return  !empty($data->$property) ? round($amount * $data->$property, 3) : "";

        }

        return '';

    }
}
