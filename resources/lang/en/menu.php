<?php
return [
    'dashboard' => 'Dashboard',
    'free_layout' => 'Free Layout Setting',
    'free_layout_index' => 'Config',
    'news_index' => 'News',
	'logout' => 'Logout',
    'inquiries_index' => 'Inquiries' ,
    'free&easy_index' => 'Free&Easy',
    'setting' => 'Setting',
    'setting_email' => 'Email',
    'setting_account' => 'Account',
];
