<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;
use Illuminate\Support\Facades\App;
use AppHelper;
use App\Helpers\LayoutHelper;
use App\Traits\UploadTrait;
use App\Http\Requests\Users as UsersValidate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Helpers\EmailHelpers;
use App\Models\Settings;

class UsersController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->response->setViewFolder('admin.settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userModel = new Users();
        $users = $userModel->where('id', Auth::user()->id)
        ->first()->toArray();
        return $this->response->title('Settings Account')
            ->view('account')
            ->data(['users' => $users])
            ->output();
    }

    /**
     * Edit template block.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersValidate $request)
    {
        if ($request->isMethod('post')) {
            $user = Auth::user();
            $old_username = $user->username;
            $post_data = $request->all();
            $new_password = $post_data['password'];
            // update status settings
            $result = $user->update([
                'username'=>$post_data['username'],
                'password'=>Hash::make($post_data['password']),
            ]);
            if ($result) {
                if (isset($post_data['send_email'])) {
                    $newUser = $user->toArray();
                    $newUser['new_password'] = $new_password;
                    $newUser['old_username'] = $old_username;
                    $settingModel = new Settings();
                    $email_arr =  $settingModel->getEmailArrayInEmailSetting('email', 'email');
                    EmailHelpers::sendNewIdPasswordToAdminMail(['email'=>$email_arr, 'users'=>$newUser]);
                }
                session()->flash('success', 'Update user successfully');
            } else {
                session()->flash('error', 'Update fail');
            }
        }
        return redirect(route('admin.settings.users'));
    }

    /**
     * Delete settings
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $id = request()->get('id');
        $type = request()->get('type');
        if (!$id) {
            return json_encode(['status'=>'error', 'message'=>'Missing id']);
        }
        if (!$type) {
            return json_encode(['status'=>'error', 'message'=>'Missing type']);
        }
        Settings::where(['id'=> $id, 'type'=>$type])->delete();
        return json_encode(['status'=>'success', 'message'=>'Delete successfully']);
    }
}
