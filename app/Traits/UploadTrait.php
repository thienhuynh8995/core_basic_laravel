<?php
namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Helpers\AppHelper;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = (!is_null($filename) ? $filename : time()) .'-' . AppHelper::generateRandomString(8) . '.'. $uploadedFile->getClientOriginalExtension(); 

        $file = $uploadedFile->storeAs($folder, $name, $disk);

        return $file ? $name : '';
    }
}