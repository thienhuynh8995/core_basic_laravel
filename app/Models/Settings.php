<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'settings';
    protected $fillable = ['key', 'value', 'type', 'description'];

    protected $setting_type = ['email'];

    public function getPublicSettings($type){
        $result = [];
        if (!empty($type)){
            $result = $this->where(['type'=>$type, 'status'=>1])->get()->toArray();
        }
        return $result;
    }

    public function getPublicSettingsWithKey($type, $key){
        $result = '';
        if (!empty($type)){
            $result = $this->where(['type'=>$type, 'key'=>$key, 'status'=>1])->first()->toArray();
        }
        return $result;
    }

    public function getEmailArrayInEmailSetting(){
        $result = $this->where(['type'=>'email', 'key'=>'email', 'status'=>1])->first()->toArray();
        $email_list = isset($result['value']) ? $result['value'] : '';
        $email_arr = preg_split("/\s*,*\s*\r\n/", $email_list);
        return $email_arr;
    }

    public function getSettings($type){
        $result = [];
        if (!empty($type)){
            $result = $this->where(['type'=>$type])->get()->toArray();
        }
        return $result;
    }

    public function checkSettingExists($type){
        return in_array($type, $this->setting_type);
    }

    public function updateSetting($type, $id, $data) {
        if (!$this->checkSettingExists($type)) {
            return ['status'=>'error', 'message'=>'There is no setting name '.$type];
        }
        $this->where(['id'=> $id, 'type'=>$type])->update($data);
        return ['status'=>'success', 'message'=>'Update successfully'];
    }

    public function addUpdateSettings($type, $post_data) {
        if (!$this->checkSettingExists($type)) {
            return ['status'=>'error', 'message'=>'There is no setting name '.$type];
        }
        $add_data = [];
        foreach ($post_data['id'] as $key => $id) {
            if (!$id){
                $add_data[] = [
                    'key'=>$post_data['key'][$key],
                    'value'=>isset($post_data['value'][$key]) ? $post_data['value'][$key] : '',
                    'description'=>isset($post_data['description'][$key]) ? $post_data['description'][$key] : '',
                    'status'=>isset($post_data['status'][$key]) ? $post_data['status'][$key] : 1,
                    'type'=>$type
                ];
            } else {
                $update_data = [
                    'key'=>$post_data['key'][$key],
                    'value'=>isset($post_data['value'][$key]) ? $post_data['value'][$key] : '',
                    'description'=>isset($post_data['description'][$key]) ? $post_data['description'][$key] : '',
                    'status'=>isset($post_data['status'][$key]) ? $post_data['status'][$key] : 1,
                ];
                $this->updateSetting($type, $id, $update_data);
            }
        }
        $this->insert($add_data);
        return ['status'=>'success', 'message'=>'Update successfully'];
    }
}
