<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class VerifyRole
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $role = $request->user()->role;
        if(($guard=='admin' && $role!=AppHelper::USER_ROLE_ADMIN) || ($guard=='client' && $role!=AppHelper::USER_ROLE_NORMAL)){
            Auth::logout();
            return redirect(route('login'))->withErrors(__('auth.not_authorized'));
        }

        return $next($request);
    }

}
