<?php
/**
 ***************************************************************************
 * Language Helpers
 ***************************************************************************
 * Created: 2018/06/21
 * This is a helpers get, check language
 *
 *
 ***************************************************************************
 * @author: thuan<n_thuan@stagegroup.jp>
 ***************************************************************************
 */

namespace App\Helpers;

use Auth;
use Session;
use App\Jobs\SendEmail;

class EmailHelpers
{
    /**
     ***************************************************************************
     ***************************************************************************
     * @author: Thien Huynh<h_thien@stagegroup.jp>
     * @param: $aDatas
     * @return:
     *
     ***************************************************************************
     */
    public static function sendTestMail(array $aDatas) {

        $sSubject = 'Test Subject';
        // Check user email has existed or not
        if (array_key_exists("email",$aDatas)) {
            $aDatas['email_template'] = 'email.test';
            $aDatas['email_subject'] = $sSubject;
            $aDatas['email_name'] = 'Test Email';
            dispatch(new SendEmail($aDatas));
        }
    }

    /**
     ***************************************************************************
     ***************************************************************************
     * @author: Thien Huynh<h_thien@stagegroup.jp>
     * @param: $aDatas
     * @return:
     *
     ***************************************************************************
     */
    public static function sendNewIdPasswordToAdminMail(array $aDatas) {

        $sSubject = 'User new information';
        // Check user email has existed or not
        if (array_key_exists("email",$aDatas)) {
            $newData = $aDatas;
            foreach ($aDatas['email'] as $email) {
                if (!empty($email)){
                    $newData['email'] = $email;
                    $newData['email_template'] = 'email.send_new_id_pass_to_admin_mail';
                    $newData['email_subject'] = $sSubject;
                    $newData['email_name'] = 'User new information';
                    dispatch(new SendEmail($newData));
                }
            }
        }
    }
}
