<table class="content-manage">
    <tbody>
        @foreach ($lang as $k=>$item)
            <tr>
                <th width="15%">{!! $attr['block_name'] !!} ({{ $item['type'] }})</th>
                <td>
                    <?php
                    if (gettype($attr['id']) == 'string' && preg_match('/^(?=(id\-)(\d+))/', $attr['id'], $new_id)){
                        $new_id = $new_id[2];
                        $temp_value = isset($attr['temp_input_arr'][$new_id]) ? $attr['temp_input_arr'][$new_id]['data_'.($k+1)] : '';
                    } else {
                        $temp_value = isset($attr['temp_input_arr'][$attr['id']]) ? $attr['temp_input_arr'][$attr['id']]['data_'.($k+1)] : '';
                    }

                    $value = old("value.title.".$attr['id'].".$k", $temp_value);
                    ?>
                    <input type="text"
                        name="{{ "value[title][".(isset($attr['id']) ? $attr['id']: '')."][]" }}"
                        placeholder="{{ isset($attr['placeholder']) ? $attr['placeholder']: 'Please input title' }}"
                        class="mb10 width-auto form-control"
                        value="{{ $value }}">
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
