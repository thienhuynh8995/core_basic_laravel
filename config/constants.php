<?php

return [
	 /*
    |--------------------------------------------------------------------------
    | Application Currency default
    |--------------------------------------------------------------------------
    |
    | This is currency which is displayed as default. 
    |
    */

    'currency' => 'VND',

    /*
    |--------------------------------------------------------------------------
    | Currency options
    |--------------------------------------------------------------------------
    |
    | This is other currencies which are displayed when user want to switch currency. ['USD',..]
    |
    */

    'currency_option' => ['USD']
];