<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyLogin
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = user();
        if (!$user || strtolower($user->status) !== 'active') {
            Auth::logout();
            return redirect(route('login'));
        }

        return $next($request);
    }
}
