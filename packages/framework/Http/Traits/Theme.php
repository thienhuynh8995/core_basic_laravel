<?php

namespace Gonosen\http\Traits;
use Theme as BaseTheme;

trait Theme
{
    /**
     * @var  Theme for the request.
     */
    public $theme = null;

    /**
     * @var  Theme layout for the request.
     */
    protected $layout = null;

    /**
     * @var Store the page title.
     */
    protected $title = null;

    /**
     * @var Store the page keyword.
     */
    protected $keyword = null;

    /**
     * @var Store the page description.
     */
    protected $description = null;

     /**
     * @var Store the page h1_tag.
     */
    protected $h1_tag = null;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function title($title)
    {

        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetaKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     *
     * @return self
     */
    public function metaKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $keyword
     *
     * @return self
     */
    public function metaDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getH1Tag()
    {
        return $this->h1_tag;
    }

    /**
     * @param mixed $keyword
     *
     * @return self
     */
    public function h1Tag($h1_tag)
    {
        $this->h1_tag = $h1_tag;

        return $this;
    }

    /**
     * @param  set the  $theme
     *
     * @return self
     */
    public function theme($theme)
    {
        $this->theme = BaseTheme::uses($theme);

        return $this;
    }

    /**
     * @return get the current $theme
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param  Theme for the request $layout
     *
     * @return self
     */
    public function layout($layout)
    {
        $this->theme->layout($layout);

        return $this;
    }

    /**
     * @return  Theme for the request
     */
    public function getLayout()
    {
        return $this->layout;
    }
}
