<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            {!! Theme::partial('message') !!}
            <div class="row logo txt_center"><a href="" ><img src="{{ asset("assets/common_img/logo.png") }}"></a></div>
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">サインインしてください</h3>
                </div>
                <div class="panel-body">
                    {!!Form::vertical_open()
                    ->id('login')
                    ->method('POST')
                    ->class('white-row')!!}
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="ユーザーID" name="email" type="text" autofocus autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="パスワード" name="password" type="password" value="">
                            </div>
                            {{-- <div class="checkbox">
                                <label>
                                    <input id="rememberme" type="checkbox" name="remember" type="checkbox" value="Remember Me">ログイン情報を保存する
                                </label>
                            </div> --}}
                            {{-- @if(get_guard() == 'admin_company.web')
                                <div style="margin-bottom:15px;"><a href="{{ route('pwchange.index') }}?type=2">パスワードをお忘れの方はこちら</a></div>
                            @endif --}}
                            <!-- Change this to a button or input when using this as a form -->
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="ログイン">
                        </fieldset>
                    {!!Form::Close()!!}
                </div>
            </div>
        </div>
    </div>
</div>


