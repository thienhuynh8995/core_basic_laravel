<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\News;
use App\Models\TemplateBlock;
use App\Models\TemplateInput;
use App\Models\NewsDetail;
use Illuminate\Support\Facades\App;
use AppHelper;
use App\Helpers\LayoutHelper;
use App\Traits\UploadTrait;
use App\Http\Requests\News as NewsValidate;
use Illuminate\Support\Facades\Storage;

class NewsController extends BaseController
{
    use UploadTrait;

    private $_uploadPath = "uploads/news/";
    private $_productName = "news";
    private $_productDetailName = "news_detail";
    private $_mainName = "news";
    private $_title = "News";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        view()->share([
            'main_name' => $this->_mainName,
            'upload_path' => $this->_uploadPath,
            'title' => $this->_title
        ]);
        $this->response->setViewFolder('admin.news');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $_productName = $this->_productName;
        $_productDetailName = $this->_productDetailName;
        $products = News::leftJoin($_productDetailName, 'news_id', 'news.id')
        ->where("$_productDetailName.lang_id", AppHelper::getLocaleIdDefault())
        ->select("$_productName.*", "$_productDetailName.lang_id", "$_productDetailName.content", "$_productDetailName.title")
        ->orderBy("$_productName.updated_at",'DESC')
        ->paginate(config('settings.limit'));
        return $this->response->title($this->_title)
            ->view('index')
            ->data(['products' => $products])
            ->output();
    }

    /**
     * Create template block.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(NewsValidate $request)
    {
        $productDetailModel = new NewsDetail();
        $productModel = new News();

        $layoutHelper = new LayoutHelper();
        $data = $layoutHelper->getLayoutData('', $productModel->getTable());
        if ($request->isMethod('post')) {

            $post_data = $request->except('_token');
            $fileName = '';
            if ($request->file('news_image')) {
                $file = $request->file('news_image');
                $fileName = $this->uploadOne($file[0], $this->_uploadPath);
            }
            $createdItem = $productModel->create([
                'status' => isset($post_data['status']) ? 1 : 0,
                'layout_status' => isset($post_data['layout_status']) ? 1 : 0,
                'image' => $fileName
            ]);
            if ($createdItem) {
                $id = $createdItem->id;
                // insert product detail
                $result = $productDetailModel->updateDataWithLang($post_data, $id);
                session()->flash('success', $result['message']);
            } else {
                session()->flash('error', 'Created fail');
            }
            return redirect(route('admin.news.index'));
        }
        return $this->response->title($this->_title . '::Add new')
            ->view('create')
            ->data($data)
            ->output();
    }

    /**
     * Edit template block.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsValidate $request, $id)
    {
        $productDetailModel = new NewsDetail();
        $productModel = new News();
        $product = $productModel->getProductsWithId($id);

        $product_detail = $product->toArray();
        $layoutHelper = new LayoutHelper();
        $data = $layoutHelper->getLayoutData($id, $productModel->getTable());
        if ($request->isMethod('post')) {
            if ($request->get('change-layout')){
                return redirect(route('admin.news.edit', $id));
            }

            $post_data = $request->except('_token');
            $fileName = $request->get('current_image') !== null ? $request->get('current_image') : '';
            if ($request->file('news_image')) {
                if($request->get('current_image')){
                    Storage::disk('public')->delete($this->_uploadPath . $request->get('current_image'));
                }
                $file = $request->file('news_image');
                $fileName = $this->uploadOne($file[0], $this->_uploadPath);
            }
            // update status product
            $productModel->where('id', $id)->update([
                'status' => isset($post_data['status']) ? 1 : 0,
                'layout_status' => isset($post_data['layout_status']) ? 1 : 0,
                'image' => $fileName
            ]);
            // update product detail with languages
            $result = $productDetailModel->updateDataWithLang($post_data, $id);
            if ($result['status'] == 'success') {
                session()->flash('success', $result['message']);
            } else {
                session()->flash('error', $result['message']);
            }
            return redirect(route('admin.news.edit', $id));
        }
        $product_arr = [];
        foreach($product_detail as $new) {
            $product_arr[$new['lang_id']] = $new;
        }
        $data['product'] = $product;
        $data['product_detail'] = $product_arr;
        $data['id'] = $id;
        return $this->response->title($this->_title . '::edit')
            ->view('create')
            ->data($data)
            ->output();
    }

    /**
     * Delete multiple product.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        $productModel = new News();
        $idArr = $request->post('idArr');
        if ($idArr == null) {
            return ['status'=>'error', 'message'=>'Missing idArr parameter'];
        }
        foreach ($idArr as $id) {
            // delete News Detail
            NewsDetail::where('news_id', $id)->delete();
            // delete News
            $product = News::where(['id'=> $id])->first();
            // delete image
            Storage::disk('public')->delete($this->_uploadPath . $product->image);

            $product->delete();
        }

        return ['status'=>'success', 'message'=>'Delete Successfully'];
    }

    /**
     * Delete product
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $productModel = new News();
        // delete News Detail
        NewsDetail::where('news_id', $id)->delete();
        // delete News
        $products = News::where(['id'=> $id])->first();
        // delete image
        Storage::disk('public')->delete($this->_uploadPath . $products->image);

        $products->delete();
        session()->flash('success', 'Delete successfully');
        return redirect(route('admin.news.index'));
    }
}
