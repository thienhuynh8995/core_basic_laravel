<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{!! Theme::getTitle() !!} :: {{trans('admin.name')}}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300'" rel='stylesheet' type='text/css'>
        {!! Theme::asset()->styles() !!}

        {!! Theme::asset()->scripts() !!}
        @section('header_styles')@show
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div style="position: fixed; top: 0px; bottom: -150px; right: 0px; left: 0px; text-align: center; line-height: 300px; z-index: 9999999; background: rgba(201, 201, 201, 0.6);" class="hidden loading-bg">
            <span>&nbsp;</span>
            <div style="line-height: 36px;">
                <img src="{{ asset("assets/common_img/lightbox/loading.gif") }}">
                <span class="text" style="width: 100%;display: inline-block;font-weight: 700;color: red;"></span>
            </div>
        </div>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/<?=get_guard('url');?>"><img style="height:40px;" src="{{ asset('assets/common_img/logo.png') }}" alt=""/></a>
                </div>
                <!-- /.navbar-header -->
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <form method="post" id="logout_frm" action="{{ route('logout') }}" >
                           @csrf
                        </form>
                        <a id="logout_btn" href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <div class="navbar-default sidebar" role="navigation">
                    {!! Theme::partial('aside') !!}
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                @if(!isset($hide_layout_message) || (isset($hide_layout_message) && $hide_layout_message==false))
                    {!! Theme::partial('message') !!}
                @endif
                {!! Theme::content() !!}
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        {!! Theme::partial('common') !!}
        {!! Theme::asset()->container('footer')->scripts() !!}
        @section('footer-helper')@show
        @section('footer-script')@show
        <script type="text/javascript">
            $("#logout_btn").click(function(){
                 $("#logout_frm").submit();
            });
        </script>
    </body>
</html>






