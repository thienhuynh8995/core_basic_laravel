<table class="content-manage">
    <tbody>
        @foreach ($lang as $k=>$item)
            <tr>
                <th width="15%">{!! $attr['block_name'] !!} ({{ $item['type'] }})</th>
                <td>
                    <?php
                    if (gettype($attr['id']) == 'string' && preg_match('/^(?=(id\-)(\d+))/', $attr['id'], $new_id)){
                        $new_id = $new_id[2];
                        $temp_value = isset($attr['temp_input_arr'][$new_id]) ? $attr['temp_input_arr'][$new_id]['data_'.($k+1)] : '';
                    } else {
                        $temp_value = isset($attr['temp_input_arr'][$attr['id']]) ? $attr['temp_input_arr'][$attr['id']]['data_'.($k+1)] : '';
                    }
                    $value = old("value.".$attr['id'].".$k", $temp_value);
                    ?>
                    <textarea class="form-control mb10 width-auto" name="value[content_area][{{ isset($attr['id']) ? $attr['id']: '' }}][]" rows="10"
                        placeholder="{{ isset($attr['placeholder']) ? $attr['placeholder']: 'Please input ' }}">{{ $value }}</textarea>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
