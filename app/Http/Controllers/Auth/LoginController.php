<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use App\Traits\RoutesAndGuards;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;
use Gonosen\Theme\ThemeAndViews;
use App\Traits\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Response\AuthResponse;

class LoginController extends Controller
{


    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use RoutesAndGuards, ThemeAndViews, ValidatesRequests;
    use AuthenticatesUsers{
        logout as performLogout;
        login as performLogin;
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->response   = resolve(AuthResponse::class);
        $this->setRedirectTo();
        $this->middleware('guest:' . $this->getGuard(), ['except' => ['logout', 'verify', 'locked', 'sendVerification']]);
        $this->setTheme();
        $breadcrumbs = [];
        $breadcrumbs[]=['url'=>'','title'=>'ログイン'];
        View::share(['breadcrumbs'=>$breadcrumbs]);
    }
    public function username(){
        $login = request()->input('email');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'username' : 'username';
        request()->merge([$field => $login]);
        return $field;
    }
    public function logout(Request $request)
    {
        $guard = $this->getGuard();
        $this->guard()->getCookieJar()->queue($this->guard()->getCookieJar()->forget($this->guard()->getRecallerName()));
        $this->guard()->logout();

        $redirectTo = config('auth.guards.'.current(explode(".", $guard)).'.redirect_after_logout');
        if($redirectTo)
            return redirect()->route($redirectTo);

        return redirect('/');
    }
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password','role');
    }
}
