<?php if(isset($errors) && count($errors)):?>
<div class="row">
    <div class="col-lg-12">
        <ul class="alert alert-danger mt20">
            <?php foreach($errors->messages() as $error):?>
                <li><?="- ".$error[0];?></li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
<?php elseif(session('error')): ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-danger mt20">
            {{ session('error') }}
        </div>
    </div>
</div>
<?php endif;?>

@if(session('success'))
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-success mt20">
                {{session('success')}}
            </div>
        </div>
    </div>
@endif

