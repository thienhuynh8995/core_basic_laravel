<!DOCTYPE html>
<html class="lockscreen">
    <head>
        <meta charset="UTF-8">
        <title>{!! Theme::getTitle() !!} :: {{config('app.name')}}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300'" rel='stylesheet' type='text/css'>
        {!! Theme::asset()->styles() !!}
        {!! Theme::asset()->scripts() !!}
        <!-- Theme -->
        <style>
            .row.logo.txt_center {
                text-align: center;
                position: relative;
                top: 50px;
            }
        </style>
    </head>
    <body class="login-page">
    {!! Theme::content() !!}
    {!! Theme::asset()->container('footer')->scripts() !!}
    @stack('script')
    </body>
</html>
