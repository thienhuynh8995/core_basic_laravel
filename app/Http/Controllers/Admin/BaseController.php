<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Gonosen\Theme\ThemeAndViews;
use App\Traits\RoutesAndGuards;
use App\Http\Response\ResourceResponse;
use App\Helpers\AppHelper;

class BaseController extends Controller
{
    use RoutesAndGuards, ThemeAndViews;

    /**
     * Initialize public controller.
     *
     * @return null
     */
    public function __construct()
    {
        if (!empty(app('auth')->getDefaultDriver())) {
            $this->middleware('auth:' . app('auth')->getDefaultDriver());
            $this->middleware('role:' . $this->getGuardRoute());
            $this->middleware('active');
        }
        AppHelper::renderDebugbar();
        $this->response = app(ResourceResponse::class);
        $this->setTheme();
    }

    public function notYetImplemented(){
        $this->response->setViewFolder('common');
        return $this->response
            ->view('not_yet_implemented')
            ->output();
    }
}
