<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        if (request()->segment(1) == 'api' || request()->segment(2) == 'api') {
            return;
        }
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));

        $this->mapSeparatedWebRoutes();
    }

    /*
     * Separate web routes
     */
    protected function mapSeparatedWebRoutes()
    {
        $this->require_all(base_path('routes/web'));
        $this->require_all(base_path('routes/admin'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        if (request()->segment(1) != 'api' || request()->segment(2) != 'api') {
            return;
        }
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));

        $this->mapSeparatedApiRoutes();
    }

    /*
     * Separate web routes
     */
    protected function mapSeparatedApiRoutes()
    {
        $this->require_all(base_path('routes/api'), 'api');
    }

    /*
     *  List all file in *dir* and include it to route
     */
    private function require_all($dir, $middleware = 'web')
    {
        if(is_file($dir)) {
            $this->includeRoute($dir, $middleware);
            return;
        }
        // require all php files
        $scan = glob("$dir/*");

        foreach ($scan as $path) {
            if (preg_match('/\.php$/', $path)) {
                $this->includeRoute($path, $middleware);
                continue;
            }
        }
    }

    /*
     *  Include file to route
     */
    private function includeRoute($path, $middleware = 'web')
    {
        if ($middleware == 'web') {
            Route::middleware($middleware)
                ->namespace($this->namespace)
                ->group($path);
            return;
        }

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group($path);
    }
}
