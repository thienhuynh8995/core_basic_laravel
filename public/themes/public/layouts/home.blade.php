<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta charset="UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ Theme::getTitle() }}</title>
    <meta name="keyword" content="{{ Theme::getMetaKeyword() }}">
    <meta name="description" content="{{ Theme::getMetaDescription() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{asset('apple-touch-icon.png')}}">
    <meta name="robots" content="INDEX,FOLLOW" />
    <meta name="format-detection" content="telephone=no" />

    {!! Theme::asset()->styles() !!}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131919353-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-131919353-1');
    </script>
    {!! Theme::asset()->scripts() !!}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" />
    <!-- JS -->
    <!-- <?php //echo DispBeforeHeadEndTag();?> -->
    <style>
        select::-ms-expand {
            /* IE 8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            /* IE 5-7 */
            filter: alpha(opacity=0);
            /* Good browsers :) */
            opacity:0;
        }
    </style>
</head>
<body id="wrapper" class="{{ isset($bodyClass) ? $bodyClass : '' }}">
    {!! Theme::partial('header') !!}
    {!! Theme::content() !!}
    {!! Theme::partial('footer') !!}
    {!! Theme::asset()->container('footer_default')->scripts() !!}
    {!! Theme::asset()->container('footer_home')->scripts() !!}
    {!! Theme::asset()->container('extra')->scripts() !!}
    <script>
        $(document).ready(function () {
            $("#tableadd").hide();
            $("#loadMore").on('click', function (e) {
                $("#tableadd").slideDown();
            });
        });
    </script>
    @section('footer-script')@show
</body>
</html>
