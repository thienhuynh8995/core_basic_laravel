<?php

// Admin  routes  for inquiries pages
Route::group([
    'namespace' => 'Admin',
    'middleware' => 'auth',
    'prefix' => set_route_guard('web'),
], function () {
	Route::get('/inquiries/', 'InquiriesController@index')->name('admin.inquiries.index');
});