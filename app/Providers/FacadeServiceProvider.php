<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any Facades.
     *
     * @return void
     */
    public function register()
    {
        // Bind facade
        $this->app->singleton('setting', function ($app) {
            return $this->app->make('App\Facades\Classes\Setting');
        });
        $this->app->singleton('user', function ($app) {
            return $this->app->make('App\Facades\Classes\User');
        });
    }

    public function provides()
    {
        return [
            'setting',
            'user'
        ];
    }
}