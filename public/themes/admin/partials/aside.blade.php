@if($isAdmin)
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav in" id="side-menu">
            <li>
                <a href="{{ route('admin.dashboard.index') }}"><i class="fa fa-sitemap fa-fw"></i> @lang('menu.dashboard')</a>
            </li>
            @if (1 == 2)
            <li>
                <a href="#"><i class="fa fa-list-alt fa-fw"></i>@lang('menu.free_layout')<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="{{ route('admin.free_layout.index')}}">@lang('menu.free_layout_index')</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            @endif
            <li>
                <a href="{{ route('admin.news.index') }}"><i class="fa fa-book fa-fw"></i> @lang('menu.news_index')</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench mr10"></i>@lang('menu.setting')<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="{{ route('admin.settings.index').'?type=email' }}">@lang('menu.setting_email')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.settings.users') }}">@lang('menu.setting_account')</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
@endif
