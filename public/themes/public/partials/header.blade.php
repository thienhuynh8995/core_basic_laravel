<header>
    <div class="clearfix">
        <div class="clearfix topnav">
            <div class="main">
                <div class="content-top">
                    <h1 class="h1name">{{ Theme::getH1Tag() }}</h1>
                    <div class="logo pull_left"><a href="/"><img src="/assets/common_img/logo.png" alt="大阪仕事市場"></a></div>
                    <div class="nav pull_right clearfix ">
                        <div class="pull_right navsp">
                            <span class="menutopsp"><img src="/assets/common_img/iconmenu.png" alt="MENU" class="icon"></span>
                            <div class="sub_menu">
                                <p class="close"><img src="/assets/common_img/iconclose.png" alt="CLOSE" class="iconclose"></p>
                                <ul>
                                    <li>
                                        <span class="">仕事市場のサービス</span>
                                        <ul class="ac_con length4">
                                            <li><a href="{{route('service')}}"><i class="fa fa-angle-right"></i>お仕事探し</a></li>
                                            <li><a href="{{route('about_bonus')}}"><i class="fa fa-angle-right"></i>お祝い金制度について</a></li>
                                            <!--<li><a href="{{route('published')}}"><i class="fa fa-angle-right"></i>掲載ご希望企業様へ</a></li>-->
                                            <li><a href="{{route('business_matching')}}"><i class="fa fa-angle-right"></i>ビジネスマッチング</a></li>
                                            <!--<li class="benefits-link"><a href="{{route('benefits')}}"><i class="fa fa-angle-right"></i>大阪ではたらくメリット</a></li>-->
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="">運営会社について</span>
                                        <ul class="ac_con length4">
                                            <li><a href="{{route('company')}}"><i class="fa fa-angle-right"></i>運営企業情報</a></li>
                                            <li><a href="{{route('faq')}}"><i class="fa fa-angle-right"></i>よくある質問</a></li>
                                            {{-- <li><a href="{{route('disclaimer')}}"><i class="fa fa-angle-right"></i>免責事項</a></li> --}}
                                            <li><a href="{{route('privacy')}}"><i class="fa fa-angle-right"></i>プライバシーポリシー</a></li>
                                            <li><a href="{{route('term')}}"><i class="fa fa-angle-right"></i>利用規約</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="">会員ログイン/新規登録</span>
                                        <ul class="ac_con length2">
                                            <li><a href="{{route('login')}}"><i class="fa fa-angle-right"></i>マイページログイン</a></li>
                                            <li><a href="{{route('user.index')}}"><i class="fa fa-angle-right"></i>新規会員登録</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{route('public.inquiry.get')}}">お問い合わせ</a>
                                    </li>
                                    <li class="">
                                        <span class="">法人様はこちら</span>
                                        <ul class="ac_con length2" style="overflow: hidden; display: none;">
                                            <li><a href="{{route('published')}}"><i class="fa fa-angle-right"></i>掲載ご希望企業様へ</a></li>
                                            <li><a href="{{ url('/admin_company') }}" target="_blank"><i class="fa fa-angle-right"></i>企業様ログインページ</a></li>
                                            <li><a href="{{route('frontend.company.entry')}}"><i class="fa fa-angle-right"></i>法人様アカウント発行フォーム</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <p class="pull_right"><a href="{{route('login')}}"><img src="/assets/common_img/iconperson.png" alt="" class="icon"></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
