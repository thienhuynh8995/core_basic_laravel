$(".search-modal-container .btn-search").click(function(){
    $('#modal-search').modal("show");
});

$btnReset = $('#item-list .btn-reset-filter');

$('#btn-apply-search').click( function() {
    oSearch = {};
    $('#form-search input, #form-search select').each( function () {
        key = $(this).attr('name');
        val = $(this).val();
        oSearch[key] = val;
    });
    oTable.api().draw();

    $btnReset.css('display', 'inline-block');
    $('#modal-search').modal("hide");
});

$btnReset.click(function (e) {
    e.preventDefault();
    $("#form-search")[ 0 ].reset();
    oSearch = {};
    $('#form-search input, #form-search textarea, #form-search select').each( function () {
        key = $(this).attr('name');
        val = $(this).val();
        oSearch[key] = val;
    });
    oTable.api().draw();

    $(this).css('display', 'none');
});

$(document).on('change', "#item-list tbody input[name^='id[]']", function(e) {
    e.preventDefault();

    aIds = [];
    $(".child").remove();
    $(this).parent().parent().removeClass('parent');
    $("input[name^='id[]']:checked").each(function(){
        aIds.push($(this).val());
    });
});

$(document).on('change', "#item-check-all", function(e) {
    e.preventDefault();
    aIds = [];
    if ($(this).prop('checked')) {
        $("input[name^='id[]']").each(function(){
            $(this).prop('checked',true);
            aIds.push($(this).val());
        });

        return;
    }else{
        $("input[name^='id[]']").prop('checked',false);
    }
});

$(document).on('click', "#manage-column", function(e) {
    $('#modal-manage-column').modal('show');
});

$(document).on('click', "#btn-reset-column", function(e) {
    $('#form-manage-column')[0].reset();

    $('#form-manage-column input[type=checkbox]').each( function () {
        var column = oTable.api().column( $(this).attr('data-column') );
        if ($(this).prop('checked')) {
            column.visible( true );
        }else{
            column.visible( false );
        }
    });
});

$(document).on('change', ".manage-column input[type=checkbox]", function(e) {
    var column = oTable.api().column( $(this).attr('data-column') );
    if ($(this).prop('checked')) {
        column.visible( true );
    }else{
        column.visible( false );
    }
});

