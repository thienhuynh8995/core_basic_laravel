try {
    window.toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $.LoadingOverlaySetup({
        color: "rgba(255, 255, 255, 0.8)",
        //image: "/img/common/loading.gif"
    });

} catch (e) {
}

$(document).ready(function () {
    try {
        $.validator.setDefaults({
            debug: true,
            success: "valid"
        });
    } catch (e) {
    }
});

window.app = {
    'win': $(window),
    'doc': $(document),
    'body': $('body')
};