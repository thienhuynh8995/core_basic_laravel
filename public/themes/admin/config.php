<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
     */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | List view for the theme
    |--------------------------------------------------------------------------
    |
    | Here you can specify which view is to be loaded for the list page
    | this can be 'list', 'grid', 'box', 'bootstrap-table' or 'data-table'
    |
    | You can specify additional views but you have to create it under
    | 'patrial/list' folder of each package that uses this theme.
    |
     */

    'listView' => 'data-table', //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
     */

    'events'  => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before'             => function ($theme) {
            // You can remove this line anytime.
            // $theme->setTitle(__('app.name'));

        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme'  => function ($theme) {
            //You may use this event to set up your assets.

            //You may use this event to set up your assets.
            $theme->asset()->usePath()->add('css1', 'vendor/bootstrap/css/bootstrap.min.css');
            $theme->asset()->usePath()->add('css2', 'vendor/metisMenu/metisMenu.min.css');
            $theme->asset()->usePath()->add('css3', 'dist/css/sb-admin-2.css');
            $theme->asset()->usePath()->add('css12', 'vendor/bootstrap-toggle-master/css/bootstrap2-toggle.min.css');
            $theme->asset()->usePath()->add('css4', 'vendor/morrisjs/morris.css');
            $theme->asset()->usePath()->add('css5', 'vendor/datepicker/datepicker.min.css');
            $theme->asset()->usePath()->add('css6', 'vendor/font-awesome/css/font-awesome.min.css');
            $theme->asset()->usePath()->add('css7', 'vendor/daterangepicker/daterangepicker.css');
            $theme->asset()->usePath()->add('css8', 'css/theme.min.css');
            $theme->asset()->usePath()->add('css9', 'css/custom.css');
            $theme->asset()->usePath()->add('css10', 'vendor/lightbox/lightbox.css');
            $theme->asset()->usePath()->add('css11', 'css/base.css');

            $theme->asset()->usepath()->add('jquery', 'vendor/jquery/jquery.min.js');
            $theme->asset()->usepath()->add('jquery-ui', 'vendor/jquery-ui/jquery-ui.min.js');
            $theme->asset()->container('footer')->usepath()->add('bootstrap', 'vendor/bootstrap/js/bootstrap.min.js');
            $theme->asset()->container('footer')->usepath()->add('mentis_menu', 'vendor/metisMenu/metisMenu.min.js');
            $theme->asset()->container('footer')->usepath()->add('admin2', 'dist/js/sb-admin-2.js');
            $theme->asset()->container('footer')->usepath()->add('common', 'js/common.js');
            $theme->asset()->container('footer')->usepath()->add('combodate', 'vendor/combodate/combodate.js');
            $theme->asset()->container('footer')->usepath()->add('momentjs', 'vendor/daterangepicker/moment.min.js');
            $theme->asset()->container('footer')->usepath()->add('daterangepicker', 'vendor/daterangepicker/daterangepicker.js');
            $theme->asset()->container('footer')->usepath()->add('jalanguage', 'vendor/daterangepicker/ja.js');
            $theme->asset()->container('footer')->usepath()->add('settings', 'js/settings.js');
            $theme->asset()->container('footer')->usepath()->add('functions', 'js/functions.js');
            $theme->asset()->container('footer')->usepath()->add('modal', 'js/modal.js');
            $theme->asset()->container('footer')->usepath()->add('lightbox', 'vendor/lightbox/lightbox.js');

            $theme->asset()->container('footer')->usepath()->add('moment', 'vendor/datepicker/moment.js');
            $theme->asset()->container('footer')->usepath()->add('datepicker', 'vendor/datepicker/datepicker.min.js');
            $theme->asset()->container('footer')->usepath()->add('datepickerja', 'vendor/datepicker/datepicker.ja.js');
            $theme->asset()->container('footer')->usepath()->add('boostrap-toggle', 'vendor/bootstrap-toggle-master/js/bootstrap2-toggle.min.js');
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [
            'auth' => function($theme)
            {
//                $theme->asset()->usePath()->add('auth', 'css/auth.css');
            }

        ],
    ],

];
