function showHidePoupButtons(showButtonClasses,hideButtonClasses){
    for(var i=0;i<showButtonClasses.length;i++){
        $('#popupModal .modal-footer button.'+showButtonClasses[i]).show();
    }
    for(var i=0;i<hideButtonClasses.length;i++){
        $('#popupModal .modal-footer button.'+hideButtonClasses[i]).hide();
    }
}
function handlePopupButton(button,yesFunction,noFunction){
    if(typeof button=='undefined' || button==POPUP_BUTTON.OK || button==POPUP_BUTTON.OK_en){
        $('#popupModal').removeAttr('data-backdrop');
        let tmp = button==POPUP_BUTTON.OK ? 'ok' : 'ok_en';
        let rev_tmp = button==POPUP_BUTTON.OK ? 'ok_en' : 'ok';
        showHidePoupButtons([tmp],[rev_tmp,'yes','no','cancel', 'ok_wd','cancel_wd']);
        if (typeof yesFunction === 'function'){
            $('#popupModal .modal-footer button.' + tmp).click(function(){
                yesFunction();
            });
        }
    }
    else {
        $('#popupModal').attr('data-backdrop', 'static');
        $('#popupModal .modal-footer button').unbind('click');
        switch(button){
            case POPUP_BUTTON.OKCANCEL:
                showHidePoupButtons(['ok','cancel'],['ok_wd','cancel_wd','ok_en','yes','no']);
                $('#popupModal .modal-footer button.ok').click(function(){
                    yesFunction();
                });
                break;
            case POPUP_BUTTON.OKCANCEL_WD:
                showHidePoupButtons(['ok_wd','cancel_wd'],['ok','cancel','ok_en','yes','no']);
                $('#popupModal .modal-footer button.ok_wd').click(function(){
                    yesFunction();
                });
                break;
            case POPUP_BUTTON.YESNO:
                showHidePoupButtons(['yes','no'],['ok_wd','cancel_wd','ok_en','ok','cancel']);
                $('#popupModal .modal-footer button.yes').click(function(){
                    yesFunction();
                });
                $('#popupModal .modal-footer button.no').click(function(){
                    noFunction();
                });
                break;
            case POPUP_BUTTON.YESNOCANCEL:
                showHidePoupButtons(['yes','no','cancel'],['ok_wd','cancel_wd','ok_en','ok']);
                $('#popupModal .modal-footer button.yes').click(function(){
                    yesFunction();
                });
                $('#popupModal .modal-footer button.no').click(function(){
                    noFunction();
                });
                break;
        }
    }
}
function showPopup(title,message,button,yesFunction,noFunction){

    if($('#popupModal').length==0){
        let html = '<div id="popupModal" class="modal fade" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title"></h4><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"></div><div class="modal-footer">';
        html += '<button type="button" class="btn btn-success ok" data-dismiss="modal">' + POPUP_BUTTON_TITLE.OK + '</button>';
        html += '<div' + (button == POPUP_BUTTON.OKCANCEL_WD ? ' style="display: block; width: 100%;"' : '') + '>';
        html += '<button type="button" class="btn cancel_wd pull-left" data-dismiss="modal">' + POPUP_BUTTON_TITLE.CANCEL_WD + '</button>';
        html += '<button type="button" class="btn btn-danger ok_wd pull-right" data-dismiss="modal">' + POPUP_BUTTON_TITLE.OK_WD + '</button>';
        html += '</div>';
        html += '<button type="button" class="btn btn-success ok_en" data-dismiss="modal">' + POPUP_BUTTON_TITLE.OK_en + '</button>';
        html += '<button type="button" class="btn btn-success yes" data-dismiss="modal">' + POPUP_BUTTON_TITLE.YES + '</button>';
        html += '<button type="button" class="btn btn-danger no" data-dismiss="modal">' + POPUP_BUTTON_TITLE.NO + '</button>';
        html += '<button type="button" class="btn btn-default cancel" data-dismiss="modal">' + POPUP_BUTTON_TITLE.CANCEL + '</button>';
        html += '</div></div></div></div>';

        $('body').append(html);
    }

    $('#popupModal .modal-title').html(title);
    $('#popupModal .modal-body').html(message);
    handlePopupButton(button,yesFunction,noFunction);
    $('#popupModal').modal('show');
}
