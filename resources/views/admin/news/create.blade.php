<link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/vendor/jquery-ui/jquery-ui.min.css">
<link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/vendor/jquery-ui/jquery-ui.structure.min.css">
<link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/vendor/jquery-ui/jquery-ui.theme.min.css">
<link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/vendor/chosen/chosen.min.css">
<style>

</style>
<form
      method="POST"
      enctype="multipart/form-data"
      id="frm-post{{ @$main_name }}"
      class="layout_management"
      autocomplete="off"
        >
<div class="row">
    <div class="col-lg-12 mt20" id="{{ @$main_name }}-create-page">
        <div class="alert alert-danger" style="display: none;" id="err-message"></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                News Management
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Content here -->
                <div class="contact_form">
                    <table class="table table-bordered">
                        <tr>
                            <th>Title</th>
                            <td>
                                @foreach ($lang as $k=>$item)
                                    @if (isset($product_detail[$item['id']]))
                                        <input type="text"
                                            name="title[id-{{ $item['id'] }}]"
                                            id="title_{{ $item['id'] }}"
                                            class="form-control mb10"
                                            value="{{ old('title.id-'.$product_detail[$item['id']]['id'], $product_detail[$item['id']]['title']) }}">
                                    @else
                                        <input type="text"
                                            name="title[{{ $item['id'] }}]" id="title_{{ $item['id'] }}"
                                            class="form-control mb10"
                                            value="{{ old('title.'.$item['id']) }}">
                                    @endif
                                    <span>({{ $item['type'] }})</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Content</th>
                            <td>
                                @foreach ($lang as $k=>$item)
                                    @if (isset($product_detail[$item['id']]))
                                        <textarea
                                            name="content[id-{{ $item['id'] }}]"
                                            class="form-control mb10 ckeditor"
                                            id="content_{{ $item['id'] }}">{{ old('content.id-'.$product_detail[$item['id']]['id'], $product_detail[$item['id']]['content']) }}</textarea>
                                    @else
                                        <textarea
                                            name="content[{{ $item['id'] }}]"
                                            class="form-control mb10 ckeditor"
                                            id="content_{{ $item['id'] }}">{{ old('content.'.$item['id']) }}</textarea>
                                    @endif
                                    <span>({{ $item['type'] }})</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>Image</th>
                            <td>
                                @php
                                    $image = old('news_image', @$product_detail[1]['image']);
                                @endphp
                                <div>
                                    <img id="image" name="news_image" width="200" @if($image) src="{{ asset("storage/$upload_path".$image) }}" @endif>
                                    @if($image)
                                    <input type="hidden" name="current_image" value="{{ $image }}">
                                    @endif
                                </div>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-default btn-file" style="background:#e8e8e8;">
                                        <span>Choose image</span>
                                        {!! Former::files('news_image')->accept('image/jpeg', 'image/png')->onchange("readURL(this)")->value($image)->raw() !!}
                                    </span>
                                    <span class="fileinput-filename"></span><span class="fileinput-new">{{isset($image) ? $image : "No file chosen"}}</span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @php
                                    $status = old('status', @$product_detail[0]['status'] !== null ? $product_detail[0]['status'] : 1);
                                @endphp
                                <input type="checkbox" name="status" data-width="50" data-height="30" {{ $status ? 'checked' : '' }} data-toggle="toggle">
                            </td>
                        </tr>
                    </table>
                    {{csrf_field()}}
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>

<div class="row mt20 mb50 pb100 clearfix">
    <div class="col-lg-12">
        <div class="col-md-4 col-xs-12 mb15 txt_left">
            <a href="{{route('admin.news.index')}}" class='btn btn-success btn-lg'>Back</a>
        </div>
        <div class="col-md-8 col-xs-12 mb15 txt_right">
            <button href="{{route('admin.news.create')}}" class='btn btn-primary btn-lg'>Submit</button>
        </div>
    </div>
</div>
</form>

@section('footer-script')
    <script src="/themes/admin/assets/vendor/chosen/chosen.jquery.js"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        // language setting, defaults en
        //CKEDITOR.config.language = 'vn';

        // display photo after selected
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image')
                        .attr('src', e.target.result)
                        // .width(153)
                    // store image src in session
                    if (typeof(Storage) !== "undefined") {
                        sessionStorage.setItem("blah_img", e.target.result);
                    }

                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
