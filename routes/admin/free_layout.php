<?php

// Admin  routes  for common pages
Route::group([
    'namespace' => 'Admin',
    // 'middleware' => 'web',
    'prefix' => set_route_guard('web'),
], function () {
	Route::match(['get', 'post'], '/free_layout/create', 'FreeLayoutController@create')->name('admin.free_layout.create');
	Route::get('/free_layout/edit', 'FreeLayoutController@edit')->name('admin.free_layout.edit');
	Route::get('/free_layout/delete', 'FreeLayoutController@index')->name('admin.free_layout.delete');
    Route::get('/free_layout/index', 'FreeLayoutController@index')->name('admin.free_layout.index');
    Route::post('/free_layout/remove-block-input', 'FreeLayoutController@removeBlockInput')->name('admin.free_layout.remove_block_input');
});
