<div class="row">
    <div class="col-lg-12 mt20">
        <div class="panel panel-default">
            <div class="panel-heading">
                Settings account Management
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Content here -->
                <form method="POST"
                    enctype="multipart/form-data"
                    action="{{route('admin.settings.users_edit')}}"
                    id="form-table">
                    {{ csrf_field() }}
                    <table width="100%" class="table general_tb table-bordered table-hover settings" id="settings">
                        <tbody>
                            <tr>
                                <th width="20%">Username</th>
                                <td><input type="text" class="form-control width-auto" name="username" value="{{ old('username', $users['username']) }}"></td>
                            </tr>
                            <tr>
                                <th width="20%">Password</th>
                                <td><input type="password" class="form-control width-auto" name="password" value=""></td>
                            </tr>
                            <tr>
                                <th width="20%">Confirm password</th>
                                <td><input type="password" class="form-control width-auto" name="password_confirm" value=""></td>
                            </tr>
                            <tr>
                                <th width="20%">Send new username and password to Admin email</th>
                                <td><input type="checkbox" name="send_email" data-width="50" data-height="30" {{ old('send_email') ? 'checked' : '' }} data-toggle="toggle"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row mt20 mb50 pb100 clearfix">
                        <div class="col-lg-12">
                            <div class="col-md-4 col-xs-12 mb15 txt_left">
                                <button class='btn btn-primary btn-md'>Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
