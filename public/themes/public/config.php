<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
     */

    'inherit' => null, //default
    /*
    |--------------------------------------------------------------------------
    | List view for the theme
    |--------------------------------------------------------------------------
    |
    | Here you can specify which view is to be loaded for the list page
    | this can be 'list', 'grid', 'box', 'bootstrap-table' or 'data-table'
    |
    | You can specify additional views but you have to create it under
    | 'patrial/list' folder of each package that uses this theme.
    |
     */

    'listView' => 'grid', //default


    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
     */

    'events'  => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before'             => function ($theme) {
            // You can remove this line anytime.
            //$theme->setTitle(__('app.name'));

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{{ $crumb["label"] }}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme'  => function ($theme) {
            //You may use this event to set up your assets.

            //You may use this event to set up your assets.

            $theme->asset()->add('css1', '/assets/css/base.css');
            $theme->asset()->add('css2', '/assets/css/top.css');
            $theme->asset()->add('css3', '/assets/css/slick.css');
            $theme->asset()->add('css4', '/assets/css/slick-themes.css');
            $theme->asset()->add('css5', '/assets/css/map.css');
            $theme->asset()->add('css5', '/assets/css/mapsearch.css');
            $theme->asset()->add('css6', '/assets/css/jquery.bxslider.css');

//            $theme->asset()->usePath()->add('vendor', 'dist/css/styles.css');
//            $theme->asset()->add('jquery', 'assets/vendor/jquery/jquery.min.js');

            //    $theme->asset()->container('footer')->usepath()->add('app', 'dist/js/app.js');
//            $theme->asset()->container('footer')->usepath()->add('common', 'js/common.js');
//            $theme->asset()->container('footer')->usepath()->add('main', 'js/main.js');

            //$theme->asset()->usepath()->add('public', 'js/theme.js');
            $theme->asset()->container('footer_default')->add('jquery', '/assets/js/jquery.min.js');
            $theme->asset()->container('footer_default')->add('common', '/assets/js/common.js');
            $theme->asset()->container('footer_default')->usepath()->add('bootstrap', 'js/bootstrap.min.js');
            $theme->asset()->container('footer_default')->usepath()->add('setting', 'js/setting.js');
            $theme->asset()->container('footer_default')->usepath()->add('modal', 'js/modal.js');

            $theme->asset()->container('footer_home')->add('slick', '/assets/js/slick.js');
            $theme->asset()->container('footer_home')->add('tile', '/assets/js/tile.js');
            $theme->asset()->container('footer_home')->add('raphael-min', '/assets/js/raphael-min.js');
            $theme->asset()->container('footer_home')->add('map', '/assets/js/map.js');
            $theme->asset()->container('footer_home')->add('jquery.bxslider', '/assets/js/jquery.bxslider.js');
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [

            'default' => function ($theme) {
            },

            'public'  => function ($theme) {
            },

            'home'    => function ($theme) {
            },

        ],

    ],

];
