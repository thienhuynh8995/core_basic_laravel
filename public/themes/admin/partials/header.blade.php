<header class="main-header">
  <!-- Logo -->
  <a href="{!!guard_url('/')!!}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">{!!trans('admin.name.short')!!}</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">{!!trans('admin.name.html')!!}</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success"> 0</span>
          </a>
          <ul class="dropdown-menu  notification">
            <li class="header">  You have 0 messages</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <div class="slimScrollDiv">

              </div>
            </li>
            <li class="footer"><a href="/admin/message/message">See All Messages</a></li>
          </ul>

        </li>
        <!-- Notifications: style can be found in dropdown.less -->
        <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-calendar"></i>
            <span class="label label-warning">  0</span>
          </a>
          <ul class="dropdown-menu notification">
            <li class="header">   You have 0 events</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <div class="slimScrollDiv">

              </div>
            </li>
            <li class="footer"><a href="/admin">View all</a></li>
          </ul>

        </li>
        <!-- Tasks: style can be found in dropdown.less -->
        <li class="dropdown tasks-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-flag-o"></i>
            <span class="label label-danger">  0</span>
          </a>
          <ul class="dropdown-menu notification">
            <li class="header"> You have 0 Tasks</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <div class="slimScrollDiv">

              </div>
            </li>
            <li class="footer"><a href="/admin">View all</a></li>
          </ul>

        </li>
      </ul>
    </div>
  </nav>
</header>