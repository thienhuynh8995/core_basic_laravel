<link media="all" type="text/css" rel="stylesheet" href="/themes/admin/assets/vendor/chosen/chosen.min.css">

<form
      method="POST"
      enctype="multipart/form-data"
      id="frm-postjob"
      autocomplete="off"
        >
<div class="row">
    <div class="col-lg-12 mt20" id="job-create-page">
        <div class="alert alert-danger" style="display: none;" id="err-message"></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                求人票の作成
            </div>
            <!-- /.panel-heading -->
            <?php
if ($job) {
	Former::populate($job);
	if (!isset($isDuplicate) || $isDuplicate) {
		Former::populateField('deadline', date('Y-m-d', time() + 60 * 60 * 24 * 31 * 3));
	} else {
		Former::populateField('deadline', date('Y-m-d', strtotime($job['deadline'])));
	}
}
?>

            <div class="panel-body">
                <!-- Content here -->
                <div class="contact_form">

                    <table class="table table-bordered">
                        <tr>
                            <th>掲載企業</th>
                            <td>
                                @if ($isAdmin)
                                    {!! Former::select('company_id')->options($companies)->raw() !!}
                                @elseif (!$isEditable)
                                    {!! Former::select('company_id')->options($companies)->select('')->placeholder("プルダウンより選択してください")->disabled()->raw() !!}
                                    {!! Form::hidden('company_id', $job['company_id']) !!}
                                @else
                                    {!! Former::select('company_id')->options($companies)->select('')->placeholder("プルダウンより選択してください")->raw() !!}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>掲載タイトル</th>
                            <td>
                                @if (!$isEditable)
                                    <?=Former::text('title')->placeholder('タイトルには職種を含めてください（30文字以内）')->disabled()->raw()?>
                                    {!! Form::hidden('title', $job['title']) !!}
                                @else
                                    <?=Former::text('title')->placeholder('タイトルには職種を含めてください（30文字以内）')->raw()?>
                                @endif
                            </td>
                        </tr>
                    </table>
                    {{csrf_field()}}

                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>
<div class="row mt20 mb50 pb100 clearfix">
    <div class="col-lg-12">
        <div class="col-md-4 col-xs-12 mb15 txt_left">
            <a href="{{route('admin.jobpost.index')}}" class='btn btn-success btn-lg'>登録せずに一覧へ戻る</a>
        </div>
        <div class="col-md-8 col-xs-12 txt_right">
            @if($showPreview)
                <input type="hidden" name="draft">
                <button type="submit" id="submitDraft" class="btn btn-warning btn-lg">下書き保存</button>
                <button id="submitButton" @if (isset($job['id']) && isset($job['status']) && $job['status'] == 0) disabled="disabled" @endif class='btn btn-primary btn-lg'>プレビュー確認</button>
            @endif

            @if($showUnpublishing)
                <button id="changeStatus" @if (isset($job['status']) && $job['status'] == 1) disabled="disabled" @endif class="btn btn-warning btn-lg">非公開に設定する</button>
            @endif
        </div>
    </div>
</div>
</form>

@section('footer-script')
    <script src="/themes/admin/assets/vendor/chosen/chosen.jquery.js"></script>

    <script>

        function formDataFileFix(formData) {
            try {
                var formKeysToBeRemoved = [];
                var es, e, pair;
                if (formData.entries){
                    for (es = formData.entries(); !(e = es.next()).done && (pair = e.value);) {
                        var fileName = null || pair[1]['name'];
                        var fileSize = null || pair[1]['size'];
                        if (fileName != null && fileSize != null && fileName == '' && fileSize == 0) {
                            formKeysToBeRemoved.push(pair[0]);
                        }
                    }
                    for (var i = 0; i < formKeysToBeRemoved.length; i++) {
                        formData.delete(formKeysToBeRemoved[i]);
                    }
                }
            }
            catch(err) {
                console.log(err.message);
            }
        }

    </script>
@endsection
