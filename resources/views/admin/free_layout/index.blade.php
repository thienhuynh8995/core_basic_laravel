<div class="row mt20">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    求人票の確認
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Content here -->
                    <div class="row mb10">
                        <div class="col-md-12 col-xs-12 txt_left">
                                <span class="mr05">
                                    <button class="btn btn-default bggrey btn_checkall">選択</button>
                                </span>
                                <span class="mr05">
                                    <button class="btn btn-default bggrey btn_clearall">選択解除</button>
                                </span>
                                @if ($isAdmin)
                                <span class="mr05">
                                    <button class="btn btn-default bggrey" id="deleteAll"><i class="fa fa-trash-o fa-fw"></i>選択した項目を削除する</button>
                                </span>
                                @endif
                        </div>
                    </div>
                    <form method="POST"
                        enctype="multipart/form-data"
                        id="form-table">
                        {{csrf_field()}}
                        <table width="100%" class="table general_tb table-striped table-bordered table-hover news_user">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th class="center"></th>
                                    <th class="center"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($temp_block as $block)
                                <tr class="odd">
                                    <td class="center"><input type="checkbox" class="check" name="id[]" value="{{ $block->id }}"></td>
                                    <td class="center">{{ $block->image }}</td>
                                    <td class="center">{{ $block->name }}</td>
                                    <td class="center" width="100">
                                        <a href="{{route('admin.free_layout.edit', $block->id)}}" class="btn btn-primary">更新</a>
                                    </td>
                                    <td class="center">
                                        <a href="{{ route('admin.free_layout.delete', $block->id) }}" class="btn btn-primary">削除</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                    <!-- pager -->
                    <div class="row mt20 mb50">
                        <div class="col-md-6 col-xs-12 mt20 mb15 txt_left">
                            <a class='btn btn-primary' href="{{route('admin.free_layout.create', [ 'is_new' => '1' ])}}">求人票を追加する</a>
                        </div>
                        <div class="col-md-6 col-xs-12 txt_left">
                            {!! $temp_block->appends(Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-4 -->
    </div>

    @section('footer-script')
    @endsection
