<div class="main seconpage__content login">
    <div class="login__index">
        <div class="titltline"><h1 class="titltline--iconclock">仕事市場会員の方のログインはこちら</h1></div>
        <?php if(isset($errors) && count($errors)):
            $validErrors = $errors->getMessages();
            $validateError = (isset($validErrors['email'][0]) && $validErrors['email'][0]==trans('auth.failed'));
        ?>
            <p class="alert">{{trans('auth.failed')}}</p>
        <?php endif;?>

        {!!Form::vertical_open()
        ->id('login')
        ->method('POST')
        ->class('white-row')!!}
            <table>
                <tbody>
                <tr>
                    <td class="<?=(isset($validateError) && $validateError?'hide-error':'');?>">
                        {!!Form::text('email',false)->placeholder('aaa@bbb.com')->autocomplete('off')!!}
                    </td>
                </tr>
                <tr>
                    <td>
                        {!!Form::password('password',false)->placeholder('パスワードを入力してください')!!}
                    </td>
                </tr>
                </tbody></table>
            <input type="hidden" name="role" value="1" />
            <div class="text-center">
                <div class="form-check">
                    <label class="form-check-label" for="rememberme"><input type="checkbox" class="form-check-input" id="rememberme" name="remember"> 次回から自動ログインする</label>
                    <div class="d-flex align-items-center justify-content-center flex-wrap">
                        <div class="frombtn--btn col-md-4">
                            <p class="btnhover text-center">
                                <button class="btn">ログイン</button>
                            </p>

                        </div>
                    </div>
                    <div class="text-link mt-3">
                        <p>※パスワードをお忘れの方は<a href="{{route('pwchange.index')}}">こちら</a></p>
                        <p>※ログインできない方は<a href="/contact">こちら</a></p>
                    </div>
                    <div class="d-flex align-items-center justify-content-center flex-wrap" style="padding-top:30px; margin-top:30px;">
                        <p class="font-weight-bold text-center col-md-8 fz18">企業アカウントでのログインはこちら</p>
                        <div class="frombtn--btn col-md-4">
                            <p class="btnhover text-center"><a target="_blank" href="{{ url('/admin_company') }}">企業ログイン</a></p>
                        </div>
                    </div>
                </div>
            </div>
        {!!Form::Close()!!}

        <div class="titltline mt-5"><h3 class="titltline--iconclock">仕事市場会員の会員でない方は、こちらから会員登録を行ってください</h3></div>
        <div class="d-flex align-items-center justify-content-center flex-wrap">
            <p class="font-weight-bold text-center col-md-8 fz18">簡単登録フォームから約1分で完了します。</p>
            <div class="frombtn--btn col-md-4">
                <p class="btnhover text-center"><a href="{{route('user.index')}}">簡易登録フォーム</a></p>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-center flex-wrap" style="padding-top:30px; margin-top:30px;">
            <p class="font-weight-bold text-center col-md-8 fz18">企業アカウント発行はこちら</p>
            <div class="frombtn--btn col-md-4">
                <p class="btnhover text-center"><a href="{{route('frontend.company.entry')}}">企業アカウント発行フォーム</a></p>
            </div>
        </div>
        <!-- End detail-->
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        // scroll when user is not login
        let href = window.location.href;
        console.log(href);
        if (/^(.)*#new-user(.)*$/.exec(href)){
            scrollToTop();
        }
    });
    function scrollToTop() {
        header_height = $('header').outerHeight(true);
        breadcumb_height = $('#breadcrumb').outerHeight(true);
        title_login_height = $('.login__index .titltline').outerHeight(true);
        login_form_height = $('.login__index #login').outerHeight(true);
        height = header_height + breadcumb_height + title_login_height + login_form_height + 48;

        if ($(window).width() < 480) {
            setTimeout(function(){
                $('html, body').animate({
                    scrollTop: height + 100
                }, 700);
            }, 500)
        }
        if ($(window).width() > 769) {
            setTimeout(function(){
                $('html, body').animate({
                    scrollTop: height
                }, 700);
            }, 500)
        }
    }
</script>
