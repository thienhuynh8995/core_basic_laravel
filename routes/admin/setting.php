<?php

// Admin  routes  for common pages
Route::group([
    'namespace' => 'Admin',
    'prefix' => set_route_guard('web'),
], function () {
    Route::group(['prefix' => 'settings'], function () {
        Route::match(['get', 'post'],'edit', 'SettingsController@edit')->name('admin.settings.edit');
        Route::match(['get', 'post'],'delete', 'SettingsController@delete')->name('admin.settings.delete');
        Route::get('index', 'SettingsController@index')->name('admin.settings.index');
        // account setting
        Route::get('account', 'UsersController@index')->name('admin.settings.users');
        Route::match(['post'],'account_edit', 'UsersController@edit')->name('admin.settings.users_edit');
    });
});
