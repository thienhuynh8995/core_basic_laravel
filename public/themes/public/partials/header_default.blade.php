<!--<?php //echo DispHeader();?>-->
<header>
    <div class="clearfix">
        <div class="clearfix topnav">
            <div class="main">
                <div class="content-top">
                    <p class="h1name">転職、就職なら大阪地域特化型の求人情報サイト【仕事市場】</p>
                    <div class="logo pull_left"><a href="/"><img src="/assets/common_img/logo.png" alt="大阪仕事市場"></a></div>
                    <div class="nav pull_right clearfix ">
                        <div class="pull_right navsp">
                            <span class="menutopsp"><img src="/assets/common_img/iconmenu.png" alt="MENU" class="icon"></span>
                            <div class="sub_menu">
                                <p class="close"><img src="/assets/common_img/iconclose.png" alt="CLOSE" class="iconclose"></p>
                                <ul>
                                    <li>
                                        <span class="">仕事市場のサービス</span>
                                        <ul class="ac_con length4">
                                            <li><a href=""><i class="fa fa-angle-right"></i>お仕事探し</a></li>
                                            <li><a href=""><i class="fa fa-angle-right"></i>お祝い金制度について</a></li>
                                            <!--<li><a href=""><i class="fa fa-angle-right"></i>掲載ご希望企業様へ</a></li>-->
                                            <li><a href=""><i class="fa fa-angle-right"></i>ビジネスマッチング</a></li>
                                            <!--<li class="benefits-link"><a href=""><i class="fa fa-angle-right"></i>大阪ではたらくメリット</a></li>-->
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="">運営会社について</span>
                                        <ul class="ac_con length4">
                                            <li><a href=""><i class="fa fa-angle-right"></i>運営企業情報</a></li>
                                            <li><a href=""><i class="fa fa-angle-right"></i>よくある質問</a></li>
                                            {{-- <li><a href="{{route('disclaimer')}}"><i class="fa fa-angle-right"></i>免責事項</a></li> --}}
                                            <li><a href=""><i class="fa fa-angle-right"></i>プライバシーポリシー</a></li>
                                            <li><a href=""><i class="fa fa-angle-right"></i>利用規約</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="">会員ログイン/新規登録</span>
                                        <ul class="ac_con length2">
                                            <li><a href=""><i class="fa fa-angle-right"></i>マイページログイン</a></li>
                                            <li><a href=""><i class="fa fa-angle-right"></i>新規会員登録</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="">お問い合わせ</a>
                                    </li>
                                    <li class="">
                                        <span class="">法人様はこちら</span>
                                        <ul class="ac_con length2" style="overflow: hidden; display: none;">
                                            <li><a href=""><i class="fa fa-angle-right"></i>掲載ご希望企業様へ</a></li>
                                            <li><a href="" target="_blank"><i class="fa fa-angle-right"></i>企業様ログインページ</a></li>
                                            <li><a href=""><i class="fa fa-angle-right"></i>法人様アカウント発行フォーム</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <p class="pull_right"><a href="{{route('login')}}"><img src="/assets/common_img/iconperson.png" alt="" class="icon"></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- NOT INCLUDE -->
    <div class="main__content">
        <div class="main">
            <div class="row align-items-center">
                <div class="main__content--img"><img src="/assets/images/iconnew.png" alt="祝い金最大50万円!!"></div>
                <div class="main__content--title"><h2 data-text="大阪エリアに特化した"><span>大阪エリア</span>に特化した<br class="block-sp"><span>お仕事探し</span>の総合サイト</h2></div>
            </div>
        </div>
    </div>
</header>
<div id="breadcrumb" class="d-none d-lg-block">
    <ul class="main d-flex justify-content-start">
        <li><a href="/">HOME</a></li>
        <?php if(isset($breadcrumbs)): foreach($breadcrumbs as $breadcrumb):?>
        <li>
            <?php if($breadcrumb['url']):?>
                <a href="<?=$breadcrumb['url'];?>">
                    <?=$breadcrumb['title'];?>
                </a>
            <?php else:?>
                <?=$breadcrumb['title'];?>
            <?php endif;?>
        </li>
        <?php endforeach;
            endif;
        ?>
    </ul>
</div>
