jQuery.fn.extend({
    dateRangeInput:function(options) {
        this.each(function () {
            var ele = $(this);
            var defaultOptions = {
                format: DATE_FORMAT,
                autoUpdateInput: false
            };
            if(typeof options != 'undefined')
                jQuery.extend(defaultOptions, options);
            ele.daterangepicker(defaultOptions, function (from_date, to_date) {
                if(from_date.format(DATE_FORMAT)==to_date.format(DATE_FORMAT))
                    ele.val(from_date.format(DATE_FORMAT));
                else
                    ele.val(from_date.format(DATE_FORMAT) + '-' + to_date.format(DATE_FORMAT));
            });
        });
    }
    ,
    comboDateInput:function(options){
        this.each(function () {
            var ele = $(this);
            var defaultOptions = {
                minYear: 1955,
                format: 'YYYY-MM-DD',
                maxYear: moment().format('YYYY'),
                customClass: 'form-control dmy'
            };
            if(typeof options!='undefined')
                jQuery.extend(defaultOptions, options);
            ele.combodate(defaultOptions);
        });
    }
});