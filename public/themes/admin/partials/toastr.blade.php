<script>
    var message = '{!! session('message') && !is_array(session('message')) ? session('message') : '' !!}';
    var status = '{!! session('status') ? session('status') : '' !!}';
    var code = '{!! session('code') ? session('code') : 0 !!}';
    app.message({
        message: message,
        statusText: message,
        status: code,
    });
</script>
