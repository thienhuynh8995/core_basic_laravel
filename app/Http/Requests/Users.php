<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use AppHelper;
use Illuminate\Support\Facades\Auth;

Class Users extends FormRequest {

    public function __construct() {
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $rules = array();
        if( $this->method() == "POST" ){
            $rules = [
                'username' => 'required|min:8|unique:users,username,'.Auth::user()->id,
                'password' => 'required|min:8',
                'password_confirm' => 'required|min:8|same:password',
            ];
        }
        return $rules;
    }

    public function messages() {
        $messages = [];
        $messages['username.required'] = 'Username field is required';
        $messages['password.required'] = 'Password field is required';
        $messages['password_confirm.same:password'] = 'Confirm password is wrong';

        return $messages;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = array();

        return $attributes;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
    }

    public function response(array $error) {
    }
}
