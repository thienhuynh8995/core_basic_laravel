<div class="row mt20">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                求人票の確認
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Content here -->
                <div class="row mb10">
                    <div class="col-md-12 col-xs-12 txt_left">
                            <span class="mr05">
                                <button class="btn btn-default bggrey btn_checkall">選択</button>
                            </span>
                            <span class="mr05">
                                <button class="btn btn-default bggrey btn_clearall">選択解除</button>
                            </span>
                            @if ($isAdmin)
                            <span class="mr05">
                                <button class="btn btn-default bggrey" id="deleteAll"><i class="fa fa-trash-o fa-fw"></i>選択した項目を削除する</button>
                            </span>
                            @endif
                    </div>
                </div>
                <form method="POST" enctype="multipart/form-data" id="form-table">
                    {{csrf_field()}}
                    <table width="100%" class="table general_tb table-striped table-bordered table-hover news_user">
                        <thead>
                            <tr>
                                <!--<th>ID</th>-->
                                <th>Name</th>
                                <th>Email</th>
                                <th>Content</th>
                                <th class="center"></th>
                                <th class="center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($news as $new)
                            <tr class="odd">
                                <!--<td class="center"><input type="checkbox" class="check" name="id[]" value="{{ $new->id }}"></td>-->
                                <td class="center">{{ $new->name }}</td>
                                <td class="center">{{ $new->email }}</td>
                                <td class="center">{{ $new->content }}</td>
                                <td class="center" width="100">
                                    <a href="{{route('admin.news.edit', $new->id)}}" class="btn btn-primary">更新</a>
                                </td>
                                <td class="center">
                                    <a href="{{ route('admin.news.delete', $new->id) }}" class="btn btn-primary">削除</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </form>
                <!-- pager -->
                <div class="row mt20 mb50">
                    <div class="col-md-6 col-xs-12 txt_left">
                        {!! $news->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
    <!-- /.col-lg-4 -->
</div>

@section('footer-script')
@endsection
