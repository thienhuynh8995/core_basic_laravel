/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location.href;
    var qIndex = url.indexOf('?');
    if(qIndex!=-1)
        url = url.substr(0,qIndex);
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    console.log('element',element);

    if(element.length==0){
        var domain =window.location.origin;
        var menus = [];
        var menu = {};
        menu.menu=domain+'/admin/company';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/company/search');
        menu.submenus.push(domain+'/admin/company_edit');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/user';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/user/search');
        menu.submenus.push(domain+'/admin/user_edit');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/jobpost';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/job/duplicate');
        menu.submenus.push(domain+'/admin/jobpost');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/entry_manage';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/entry_manage');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/scout';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/scout/detail');
        menu.submenus.push(domain+'/admin/scout/message');
        menu.submenus.push(domain+'/admin/scout_message/mail_preview');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/scout_message_sent';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/scout_message_sent');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/scout_message_reply';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/scout_message_reply');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/scout_manage';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/scout_manage');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/mail_template';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/mail_template');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/recruitment_feature';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/recruitment_feature');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/bonus_manage';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/bonus_manage');
        menus.push(menu);

        var menu = {};
        menu.menu=domain+'/admin/top_image';
        menu.submenus = [];
        menu.submenus.push(domain+'/admin/top_image');
        menus.push(menu);


        console.log('url',url);
        console.log('menus',menus);
        for(var i=0;i<menus.length;i++){
            var subMenus = menus[i].submenus;
            for(var j=0;j<subMenus.length;j++){
                if(url.indexOf(subMenus[j])!=-1){
                    var menuURL = menus[i].menu;
                    element = $('ul.nav a').filter(function() {
                        return this.href == menuURL;
                    }).addClass('active').parent();
                }

            }
        }
        console.log('element',element);
    }

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});
