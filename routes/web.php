<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*** Set the locale and put it in session ***/
Route::get('locale/{locale}', function ($locale) {
	\Session::put('locale', $locale);
	return redirect()->back();

})->name('locale');

/*** Set currency in session ***/
Route::get('currency/{currency}', function ($locale) {
	\Session::put('currency', $locale);
	return redirect()->back();

})->name('currency');

Route::group(['prefix' => (set_route_guard('web')=='web'?'':set_route_guard('web'))], function () {
    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});
Route::get('/', 'HomeController@index')->name('home');
