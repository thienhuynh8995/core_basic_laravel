<?php

// Admin  routes  for common pages
Route::group([
    'namespace' => 'Admin',
    // 'middleware' => 'web',
    'prefix' => set_route_guard('web'),
], function () {
	Route::get('/', 'DashboardController@index')->name('admin.index');
	Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard.index');
});
