<style>
    .update-status{
        border-radius: 100%;
        width: 30px;
        height: 30px;
        padding: 0;
        text-align: center;
    }
    .update-status i{
        font-size: 21px;
    }
</style>
<?php
$index = 0;
$oldInput = session()->getOldInput();
?>
<div class="row">
    <div class="col-lg-12 mt20">
        @if($validSetting)
        <div class="panel panel-default">
            <div class="panel-heading">
                Settings {{ $type }} Management
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Content here -->
                <form method="POST"
                    enctype="multipart/form-data"
                    action="{{route('admin.settings.edit').'?type='.$type}}"
                    id="form-table">
                    {{ csrf_field() }}
                    <table width="100%" class="table general_tb table-striped table-bordered table-hover settings" id="settings">
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th width="40%">Value</th>
                                <th width="40%">Description</th>
                                <th>Status</th>
                                <th class="center"></th>
                                <th class="center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($settings as $key => $item)
                            <?php
                            $index = $key+1;
                            ?>
                            <tr class="odd">
                                <td><input type="text" name="key[]" value="{{ old("key.$key", $item['key']) }}" style="font-weight: 700;"></td>
                                <td><textarea name="value[]" style="width:100%">{!! old("value.$key", $item['value'])!!}</textarea></td>
                                <td><textarea name="description[]" style="width:100%">{!! old("description.$key", $item['description'])!!}</textarea></td>
                                <td class="center">
                                    <button type="button" title="{{ old("status.$key", $item['status'])? 'TURN OFF' : 'TURN ON' }}" class="btn {{ old("status.$key", $item['status'])? 'btn-success' : 'btn-default' }} update-status">
                                        <i class="fa {{ old("status.$key", $item['status'])? 'fa-check' : 'fa-circle-o' }}"></i>
                                    </button>
                                    <input name="status[]" type="hidden" value="{{ old("status.$key", $item['status'])}}">
                                </td>
                                <td class="center">
                                    <button type="button" title="Add new" class="btn btn-primary add-new"><i class="fa fa-plus"></i></button>
                                </td>
                                <td class="center">
                                    <input type="hidden" name="id[]" value="{{ old("id.$key", $item['id'])}}">
                                    <button type="button" title="Delete" data-id="{{ old("id.$key", $item['id'])}}" class="btn btn-danger delete"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                        @empty
                            @if (empty($oldInput))
                            <tr class="odd">
                                <td><input type="text" name="key[]" value="" style="font-weight: 700;"></td>
                                <td><textarea name="value[]" style="width:100%"></textarea></td>
                                <td><textarea name="description[]" style="width:100%"></textarea></td>
                                <td class="center">
                                    <button type="button" title="TURN OFF" class="btn btn-default update-status">
                                        <i class="fa fa-circle-o"></i>
                                    </button>
                                    <input name="status[]" type="hidden">
                                </td>
                                <td class="center">
                                    <button type="button" title="Add new" class="btn btn-primary add-new"><i class="fa fa-plus"></i></button>
                                </td>
                                <td class="center">
                                    <input type="hidden" name="id[]" value="">
                                    <button type="button" title="Delete" class="btn btn-danger delete"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            @endif
                        @endforelse
                        @if (!empty($oldInput))
                            @for ($i = $index; $i < count($oldInput['key']); $i++)
                                <tr class="odd">
                                    <td><input type="text" name="key[]" value="{{ old("key.$i") }}" style="font-weight: 700;"></td>
                                    <td><textarea name="value[]" style="width:100%">{!! old("value.$i")!!}</textarea></td>
                                    <td><textarea name="description[]" style="width:100%">{!! old("description.$i")!!}</textarea></td>
                                    <td class="center">
                                        <button type="button" title="{{ old("status.$i")? 'TURN OFF' : 'TURN ON' }}"
                                            class="btn {{ old("status.$i")? 'btn-success' : 'btn-default' }} update-status">
                                            <i class="fa {{ old("status.$i")? 'fa-check' : 'fa-circle-o' }}"></i>
                                        </button>
                                        <input name="status[]" type="hidden" value="{{ old("status.$i")}}">
                                    </td>
                                    <td class="center">
                                        <button type="button" title="Add new" class="btn btn-primary add-new"><i class="fa fa-plus"></i></button>
                                    </td>
                                    <td class="center">
                                        <input type="hidden" name="id[]" value="">
                                        <button type="button" title="Delete" class="btn btn-danger delete"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                            @endfor
                        @endif
                        </tbody>
                    </table>
                    <div class="row mt20 mb50 pb100 clearfix">
                        <div class="col-lg-12">
                            <div class="col-md-4 col-xs-12 mb15 txt_left">
                                <button class='btn btn-primary btn-md'>Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.panel-body -->
        </div>
        @else
        <p>There is no setting "{{ $type }}" exists</p>
        @endif
    </div>
    <!-- /.col-lg-4 -->
</div>

@section('footer-script')
<script>
$(function(){
    $('body')
    // add new function
    .on('click', '.add-new', function(){
        let $this = $(this);
        let $tbody = $('#settings tbody');
        let $tr = $tbody.find('tr:last-child').clone(true);
        // reset data
        $tr.find('.delete').removeAttr('data-id');
        $tr.find('[name="id[]"]').val('');
        changeStatus($tr, 1);
        $tbody.append($tr);
    })
    // delete function
    .on('click', '.delete', function(){
        let $this = $(this);
        let id = $this.attr('data-id');
        if ($('#settings tbody tr').length == 1) {
            showPopup('Warning', 'You can\'t remove the first row', POPUP_BUTTON.OK);
        } else {
            if (id) {
                showPopup('Warning', 'Are you sure to delete?', POPUP_BUTTON.YESNO, function(){
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.settings.delete') }}",
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        data: {
                            id: id,
                            type: "{{ $type }}"
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response['status'] == 'success') {
                                $this.closest('tr').remove();
                            }
                        }
                    });
                })
            } else {
                $this.closest('tr').remove();
            }
        }
    })
    // change status function
    .on('click', '.update-status', function(){
        let $this = $(this);
        let $tr = $this.closest('tr');
        changeStatus($tr);
    })

    function changeStatus($tr, status=''){
        let $update_btn = $tr.find('.update-status');
        let $status = $update_btn.next('[name="status[]"]');
        let current_status = status !== '' ? (status ? 0 : 1) : parseInt($status.val());
        if (current_status){
            $update_btn.removeClass('btn-success').addClass('btn-default').attr('title', 'TURN ON');
            $update_btn.find('i').removeClass('fa-check').addClass('fa-circle-o');
            current_status = 0;
        } else {
            $update_btn.removeClass('btn-default').addClass('btn-success').attr('title', 'TURN OFF');
            $update_btn.find('i').removeClass('fa-circle-o').addClass('fa-check');
            current_status = 1;
        }
        $status.val(current_status);
    }
})
</script>
@endsection
