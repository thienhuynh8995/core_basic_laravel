<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ Theme::getTitle() }} - {{config('app.name')}}</title>
    <meta name="robots" content="NOINDEX,NOFOLLOW">
    <meta name="keyword" content="{{ Theme::getMetaKeyword() }}">
    <meta name="description" content="{{ Theme::getMetaDescription() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{asset('apple-touch-icon.png')}}">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link media="all" type="text/css" rel="stylesheet" href="/assets/css/base.css">
    <link media="all" type="text/css" rel="stylesheet" href="/assets/css/content.css">
    <link media="all" type="text/css" rel="stylesheet" href="/assets/css/form.css">
    <link media="all" type="text/css" rel="stylesheet" href="/assets/css/custom.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131919353-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-131919353-1');
    </script>

    <!-- JS -->
    {!! Theme::asset()->scripts() !!}

    <!-- JS -->
    <!-- <?php //echo DispBeforeHeadEndTag();?> -->
    <style>
        select::-ms-expand {
            /* IE 8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            /* IE 5-7 */
            filter: alpha(opacity=0);
            /* Good browsers :) */
            opacity:0;
        }
    </style>
</head>
<body id="wrapper" class="seconpage <?php echo isset($bodyClass)?$bodyClass:'';?>">
{!! Theme::partial('header_default') !!}
{!! Theme::content() !!}
{!! Theme::partial('footer') !!}
{!! Theme::asset()->container('footer_default')->scripts() !!}
{!! Theme::asset()->container('footer')->scripts() !!}
{!! Theme::asset()->container('extra')->scripts() !!}
<script>
    $(document).ready(function () {
        $("#tableadd").hide();
        $("#loadMore").on('click', function (e) {
            $("#tableadd").slideDown();
        });
    });
</script>
</body>
</html>
