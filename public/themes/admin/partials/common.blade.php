<script>
    window.trans = {
        common: {
            'ok': 'OK',
            'yes': 'はい',
            'no': 'いいえ',
            'error': 'エラー',
            'success': '成功',
            'warning': '警告',
            'info': 'Info',
        }
    }
</script>