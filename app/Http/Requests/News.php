<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use AppHelper;

Class News extends FormRequest {

    public function __construct() {
        $this->lang = AppHelper::getPublicLangs();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $rules = array();
        if( $this->method() == "POST" ){
            $rules = [
                'title.*' => 'required',
                'content.*' => 'max:1000',
            ];

        }
        return $rules;
    }

    public function messages() {
        $messages = [];

        return $messages;
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attributes = array();

        foreach ($this->lang as $lang) {
            $attributes['title.id-'.$lang['id'] . '.required'] = $lang['description'].' Title';
            $attributes['title.'.$lang['id']] = $lang['description'].' Title';
            $attributes['content.id-'.$lang['id']] = $lang['description'].' Content';
            $attributes['content.'.$lang['id']] = $lang['description'].' Content';
        }

        return $attributes;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
    }

    public function response(array $error) {
    }
}
